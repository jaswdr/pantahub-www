FROM kkarczmarczyk/node-yarn as builder

ARG NS=www

WORKDIR /builder

COPY . .

RUN set -a && REACT_APP_REVISION=$(git describe --tags)-$NS && . /builder/env.$NS && set +a && yarn && yarn build

FROM nginx

COPY --from=builder /builder/build /usr/share/nginx/html
RUN sed -i "/index.*index.html.*/ a \
\        try_files \$uri /index.html;" /etc/nginx/conf.d/default.conf


