/* eslint-disable camelcase */
/*
 * Copyright (c) 2017-2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import fetch from 'isomorphic-fetch'

// FIXME: use env var here
const API_URL = process.env.REACT_APP_API_URL || 'https://api.pantahub.com'
const AUTH_STATUS = `${API_URL}/auth/auth_status`
const LOGIN_URL = `${API_URL}/auth/login`
const ACCOUNTS_URL = `${API_URL}/auth/accounts`
const DASH_URL = `${API_URL}/dash/`
const SUMMARY_URL = `${API_URL}/trails/summary`
const DEVS_URL = `${API_URL}/devices`
const OAUTH2_AUTHORIZE_URL = `${API_URL}/auth/authorize`
const OAUTH2_CODE_URL = `${API_URL}/auth/code`
const AUTH_PASSWORD_RECOVER = `${API_URL}/auth/recover`
const AUTH_PASSWORD_URL = `${API_URL}/auth/password`

const trailObjectsURL = (id, rev) =>
  `${API_URL}/trails/${id}/steps/${rev}/objects`

const tailsDeviceSummaryUrl = (id) =>
  `${API_URL}/trails/${id}/summary`

const devLogsURL = (id, after, page = 500, sort = '-time-created') =>
  `${API_URL}/logs/?dev=prn:::devices:/${id}&after=${after}&page=${page}&sort=${sort}`

const devStepsURL = (id, status = '') =>
  `${API_URL}/trails/${id}/steps?progress.status=${status}`

const verifyAccountURL = (id, challenge) =>
  `${API_URL}/auth/verify?id=${id}&challenge=${challenge}`

// Helpers
const getOauth2AuthorizeUrlByType = (type) => {
  switch (type) {
    case 'code':
      return OAUTH2_CODE_URL
    default:
      return OAUTH2_AUTHORIZE_URL
  }
}

const _requestContentType = async (
  url,
  token = '',
  contentType = 'application/json',
  method = 'GET',
  body = {}
) => {
  let headers = {
    'Content-Type': contentType
  }

  if (token) headers['Authorization'] = `Bearer ${token}`

  let options = {
    method,
    headers: headers
  }

  if (method !== 'GET' && method !== 'HEAD') {
    options['body'] = JSON.stringify(body)
  }

  return fetch(url, options)
}

const _requestJSON = async (url, method = 'GET', token = '', body = {}) => {
  const response = await _requestContentType(
    url,
    token,
    'application/json',
    method,
    body
  )

  const json = await response.json()

  return {
    ok: response.ok,
    redirected: response.redirected,
    headers: response.headers,
    status: response.status,
    json
  }
}

const _getJSON = async (url, token) =>
  _requestJSON(url, 'GET', token)

const _postJSON = async (url, token, body) =>
  _requestJSON(url, 'POST', token, body)

const _putJSON = async (url, token, body) =>
  _requestJSON(url, 'PUT', token, body)

const _patchJSON = async (url, token, body) =>
  _requestJSON(url, 'PATCH', token, body)

const _delete = async (url, token) =>
  _requestJSON(url, 'DELETE', token)

// Auth API
export const login = async (username, password) =>
  _postJSON(LOGIN_URL, '', { username, password })

export const register = async (nick, email, password, captchaToken, encrypted) =>
  _postJSON(ACCOUNTS_URL, '', { nick, email, password, captcha: captchaToken, 'encrypted-account': encrypted })

export const verifyAccount = async (id, challenge) =>
  _getJSON(verifyAccountURL(id, challenge))

export const getUserData = async (token) =>
  _getJSON(AUTH_STATUS, token)

export const requestPasswordRecover = async (email) =>
  _postJSON(AUTH_PASSWORD_RECOVER, undefined, { email })

export const postPassword = async (token, password) =>
  _postJSON(AUTH_PASSWORD_URL, undefined, { token, password })

// Dashboard API
export const getDashData = async token => _getJSON(DASH_URL, token)

export const getDevsData = async token => _getJSON(SUMMARY_URL, token)

// Device API
export const getDeviceData = async (token, id) =>
  _getJSON(`${DEVS_URL}/${id}`, token)

export const getDeviceSummary = async (token, id) =>
  _getJSON(tailsDeviceSummaryUrl(id), token)

export const getDeviceSteps = async (token, id, status) =>
  _getJSON(devStepsURL(id, status), token)

export const getDeviceObjects = async (token, id, rev) =>
  _getJSON(trailObjectsURL(id, rev), token)

export const getDeviceLogs = async (
  token,
  id,
  after = '1970-01-01T00:00:00.0Z',
  page = 2000,
  sort = '-time-created'
) => _getJSON(devLogsURL(id, after, page, sort), token)

export const deleteDevice = async (token, id) =>
  _delete(`${DEVS_URL}/${id}`, token)

export const publishDevice = async (token, id) =>
  _putJSON(`${DEVS_URL}/${id}/public`, token)

export const unPublishDevice = async (token, id) =>
  _delete(`${DEVS_URL}/${id}/public`, token)

export const claimDevice = async (token, id, secret) =>
  _putJSON(`${DEVS_URL}/${id}?challenge=${secret}`, token)

export const setDeviceMetadata = async (token, id, meta, type = 'user-meta') =>
  _putJSON(`${DEVS_URL}/${id}/${type}`, token, meta)

export const fetchFile = async (token, url) =>
  _requestContentType(url, token, 'text/plain; charset=utf-8')

export const patchDevice = async (token, id, payload) =>
  _patchJSON(`${DEVS_URL}/${id}`, token, payload)

export const postOauth2Authorize = async (service, scopes, redirect_uri, state, token, response_type) =>
  _postJSON(getOauth2AuthorizeUrlByType(response_type), token, { service, scopes, redirect_uri, state, response_type })
