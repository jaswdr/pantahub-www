import last from 'lodash.last'
import merge from 'lodash.merge'
import sortBy from 'lodash.sortby'
import dayjs from 'dayjs'

import {
  getDeviceData,
  getDeviceSteps,
  getDeviceLogs,
  getDeviceObjects,
  getDeviceSummary
} from './api'

import { throwErrorIf, HTTP_STATUS } from '../lib/api.helpers'
import { resolvePath } from '../lib/object.helpers'

const authErrors = [
  HTTP_STATUS.FORBIDDEN,
  HTTP_STATUS.UNAUTHORIZED
]
const keyBlacklist = ['#spec']
const expandableExtensions = ['json']

function isUploaded (summaryResponse) {
  return resolvePath(summaryResponse, 'json.state-sha', '').length > 0
}

async function getLogsData (token, deviceId, fromRefresher) {
  const logsResponse = await getDeviceLogs(
    token,
    deviceId,
    fromRefresher ? window.__PH_CDLLETS : undefined,
    fromRefresher
      ? parseInt(process.env.REACT_APP_LOGS_WARM_CHUNK) || 500
      : parseInt(process.env.REACT_APP_LOGS_COLD_CHUNK) || 1000,
    fromRefresher ? 'time-created' : '-time-created'
  )

  throwErrorIf(logsResponse)(authErrors)

  return resolvePath(logsResponse, 'json.entries', [])
}

async function getAllDataFromApi (args) {
  const { token, deviceId, device, refresh, revision } = args
  let devResponse = {}
  let stepsResponse = {}
  let summaryResponse = {}
  let stateObjectsResponse = {}
  if (!refresh) {
    return {
      dev: device,
      steps: device.history.steps
    }
  }

  // fetch device information and enrich it
  [devResponse, summaryResponse] = await Promise.all([
    getDeviceData(token, deviceId),
    getDeviceSummary(token, deviceId)
  ])

  const rev = revision || resolvePath(summaryResponse, 'json.progress-revision')

  throwErrorIf(devResponse, summaryResponse)(authErrors)

  // Only Load steps and logs if device upload all files
  if (isUploaded(summaryResponse)) {
    [stepsResponse, stateObjectsResponse] = await Promise.all([
      getDeviceSteps(token, deviceId, '{"$ne":""}'),
      getDeviceObjects(token, deviceId, rev)
    ])
  }

  throwErrorIf(stepsResponse, stateObjectsResponse)(authErrors)

  return {
    dev: devResponse.json,
    summary: summaryResponse.json,
    steps: resolvePath(stepsResponse, 'json', []),
    objects: resolvePath(stateObjectsResponse, 'json', [])
  }
}

function getRevisions (steps = [], revision) {
  const lastRevision = resolvePath(steps, `${steps.length - 1}.rev`, 0)
  const parsedRevision = parseInt(revision, 10)
  const rev = isNaN(parsedRevision) ? lastRevision : parsedRevision
  const revIndex = steps.findIndex(x => x.rev === rev)
  return {
    lastRevision,
    rev,
    revIndex
  }
}

export function buildCurrentStep (steps, objects = [], revIndex) {
  const currentStep = Object.assign({}, resolvePath(steps, `${revIndex}`, {}))

  currentStep.rawState = merge({}, currentStep.state)

  currentStep.state = Object.assign(
    {},
    Object.entries(currentStep.state || {}).reduce((acc, [key, value]) => {
      const extension = last((key || '').split('.'))
      const isExpandable = expandableExtensions.indexOf(extension) >= 0
      acc[key] = {
        extension,
        isExpandable,
        hidden: keyBlacklist.indexOf(key) !== -1,
        value
      }
      return acc
    }, {})
  )

  // TODO: implement an objects cache to only request the new or missing objects here
  // or to completely omit this request if there were no changes in files between revisions
  const stateURLs = objects.reduce((acc, object) => {
    // FIXME: use env var here
    acc[object.id] = `${object['signed-geturl']}`
    return acc
  }, {})

  // update current step state with download urls
  currentStep.state = Object.assign(
    {},
    Object.entries(currentStep.state).reduce((acc, [key, spec]) => {
      acc[key] = {
        url: stateURLs[spec.value] || null,
        ...spec
      }
      return acc
    }, {})
  )

  return currentStep
}

function consolidateDeviceData (args) {
  const { dev, lastRevision, steps, rev, revIndex, currentStep, summary } = args
  return Object.assign({}, dev, {
    summary,
    history: {
      revision: rev,
      lastRevision,
      steps,
      revIndex,
      currentStep
    }
  })
}

export async function getDeviceLogsService (token, deviceId, fromRefresher) {
  const newLogs = await getLogsData(token, deviceId, fromRefresher)

  if (!fromRefresher) {
    newLogs.reverse()
  }

  const devInitNewLogs = sortBy(newLogs || [], 'tsec')
  const devInitNewestLogEntry = devInitNewLogs.slice(-1)
  const devInitNewestLogEntryTime = (devInitNewestLogEntry.length === 1
    ? dayjs(devInitNewestLogEntry[0]['time-created'])
    : dayjs(window.__PH_CDLLETS)
  ).toISOString()

  window.__PH_CDLLETS = devInitNewestLogEntryTime

  return {
    newLogs: devInitNewLogs,
    devInitNewestLogEntryTime
  }
}

export async function getAllDeviceData (args) {
  const {
    revision
  } = args
  const {
    dev,
    steps,
    summary,
    objects
  } = await getAllDataFromApi(args)
  const { lastRevision, rev, revIndex } = getRevisions(steps, revision)
  const currentStep = buildCurrentStep(steps, objects, revIndex)
  const consolidatedDevice = consolidateDeviceData({
    dev,
    lastRevision,
    rev,
    revIndex,
    currentStep,
    steps,
    summary
  })
  return {
    consolidatedDevice
  }
}
