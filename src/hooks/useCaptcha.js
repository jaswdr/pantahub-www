import { useRef, useState } from 'react'

export default function useCaptcha () {
  const captchaRef = useRef()
  const [captchaToken, setCaptchaToken] = useState(null)

  const onloadCallback = () => {}

  const verifyCallback = (response) => {
    setCaptchaToken(response)
  }

  const expiredCallback = () => {
    if (captchaRef.current) {
      captchaRef.current.reset()
    }
    setCaptchaToken(false)
  }

  return {
    captchaRef,
    captchaToken,
    onloadCallback,
    verifyCallback,
    expiredCallback
  }
}
