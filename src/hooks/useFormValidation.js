/* global FormData */
import { useState } from 'react'

export default function useFormValidation () {
  const [formState, setState] = useState({
    errors: {},
    isValid: true,
    wasValidated: false
  })

  const onSubmit = (event) => {
    event.preventDefault()
    const form = event.target
    const isValid = form.checkValidity()
    const formData = new FormData(form)
    const data = Array.from(formData.keys()).reduce((acc, key) => {
      acc[key] = formData.get(key)
      return acc
    }, {})
    const validationMessages = Array.from(formData.keys()).reduce((acc, key) => {
      acc[key] = form.elements[key].validationMessage
      return acc
    }, {})
    const newState = {
      wasValidated: true,
      errors: validationMessages,
      isValid
    }
    setState(newState)
    return { ...newState, data, form }
  }
  const onInput = (event) => {
    const input = event.target
    const isValid = input.checkValidity()
    setState({
      ...formState,
      wasValidated: true,
      errors: { ...formState.errors, [input.name]: input.validationMessage },
      isValid
    })
  }
  const hasError = (field) => !!formState.errors[field]
  const getError = (field) => formState.errors[field]
  return {
    formState,
    onSubmit,
    onInput,
    hasError,
    getError
  }
}
