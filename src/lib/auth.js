/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect'
import locationHelperBuilder from 'redux-auth-wrapper/history4/locationHelper'

import { loginPath, userDashboardPath } from '../router/routes'

export const tokenKey = '_ph_token'

const locationHelper = locationHelperBuilder({})

// Selectors

const authenticatedSelector = state => state.auth.token !== null
const notAuthenticatedSelector = state => state.auth.token === null
const authenticatingSelector = state => state.auth.gettingToken

// HOCs

export const userIsAuthenticated = connectedRouterRedirect({
  redirectPath: loginPath,
  authenticatedSelector,
  authenticatingSelector,
  wrapperDisplayName: 'UserIsAuthenticated'
})
export const userIsNotAuthenticated = connectedRouterRedirect({
  redirectPath: (state, props) =>
    locationHelper.getRedirectQueryParam(props) ||
    `${userDashboardPath}/${state.auth.username}`,
  allowRedirectBack: false,
  authenticatedSelector: notAuthenticatedSelector,
  authenticatingSelector,
  wrapperDisplayName: 'UserIsNotAuthenticated'
})
