export const HTTP_STATUS = {
  FORBIDDEN: 403,
  UNAUTHORIZED: 401,
  CREATED: 201,
  OK: 200
}

function checkFor (response, statuses = [HTTP_STATUS.UNAUTHORIZED]) {
  return statuses.indexOf(response.status) >= 0
}

export function throwErrorIf (...responses) {
  return (statuses = [HTTP_STATUS.UNAUTHORIZED]) => {
    for (const response of responses) {
      if (checkFor(response, statuses)) {
        throw response
      }
    }
  }
}

export function isResponseError (...responses) {
  return (statuses = [HTTP_STATUS.UNAUTHORIZED]) => {
    return responses.some(response => checkFor(response, statuses))
  }
}

export function ErrorIfNotAuthorized (...responses) {
  return throwErrorIf(...responses)([HTTP_STATUS.UNAUTHORIZED])
}

export function notAuthorized (...responses) {
  return isResponseError(...responses)([HTTP_STATUS.UNAUTHORIZED])
}

export function ErrorIfIsForbidden (...responses) {
  return throwErrorIf(...responses)([HTTP_STATUS.FORBIDDEN])
}

export function isForbidden (...responses) {
  return isResponseError(...responses)([HTTP_STATUS.FORBIDDEN])
}

export function parseImplicitData (hash) {
  return (hash.match(/([^#=&]+)(=([^&]*))?/g) || []).reduce((acc, val) => {
    const [key, value] = val.split('=')
    acc[key] = value
    return acc
  }, {})
}
