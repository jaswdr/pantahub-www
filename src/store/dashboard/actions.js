/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import * as Types from './types'

import { getDashData } from '../../services/api'

import { logout } from '../auth/actions'

export const initializeDashboardInProgress = () => ({
  type: Types.DASH_INIT_INPROGR
})
export const initializeDashboardSuccess = data => ({
  type: Types.DASH_INIT_SUCCESS,
  data
})
export const initializeDashboardFailure = error => ({
  type: Types.DASH_INIT_FAILURE,
  error
})
export const resetDashboard = () => ({
  type: Types.DASH_RESET
})

export const initializeDashboard = token => async dispatch => {
  dispatch(initializeDashboardInProgress())

  try {
    const dashData = await getDashData(token)

    if (!dashData.ok) {
      // dispatch(initializeDashboardFailure(dashData.json));
      dispatch(logout())
    } else dispatch(initializeDashboardSuccess(dashData.json))
  } catch (err) {
    dispatch(initializeDashboardFailure({ code: 0, message: err }))
    throw err
  }
}
