/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import * as Types from './types'

const dashInitialState = {
  data: {},
  error: null,
  loading: false
}

export const dashReducer = (state = dashInitialState, action) => {
  switch (action.type) {
    case Types.DASH_INIT_INPROGR:
      return Object.assign({}, state, {
        loading: true
      })
    case Types.DASH_INIT_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        data: action.data
      })
    case Types.DASH_INIT_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error
      })
    case Types.DASH_RESET:
      return Object.assign({}, dashInitialState)
    default:
      return state
  }
}
