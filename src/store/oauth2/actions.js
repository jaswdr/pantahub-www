/* eslint-disable camelcase */
/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as Types from './types'

import { postOauth2Authorize } from '../../services/api'

export const oauth2AuthorizeInProgress = () => ({
  type: Types.OAUTH2_AUTHORIZE_INPROGR
})

export const oauth2AuthorizeSuccess = response => ({
  type: Types.OAUTH2_AUTHORIZE_SUCCESS,
  response
})

export const oauth2AuthorizeFailure = error => ({
  type: Types.OAUTH2_AUTHORIZE_FAILURE,
  error
})

export const oauth2AuthorizeToken = (props) => async dispatch => {
  const { client_id, scope, redirect_uri, oauth2State, token, response_type } = props
  dispatch(oauth2AuthorizeInProgress())
  try {
    const resp = await postOauth2Authorize(client_id, scope, redirect_uri, oauth2State, token, response_type)
    if (!resp.ok) dispatch(oauth2AuthorizeFailure(resp.json))
    else dispatch(oauth2AuthorizeSuccess(resp.json))
  } catch (err) {
    dispatch(oauth2AuthorizeFailure({ code: 0, message: err }))
    throw err
  }
}
