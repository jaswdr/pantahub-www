/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

export const DEVS_INIT_INPROGR = 'devs/init-inprogress'
export const DEVS_INIT_SUCCESS = 'devs/init-success'
export const DEVS_INIT_FAILURE = 'devs/init-failure'
export const DEVS_RESET = 'devs/reset'
export const DEVS_SET_SEARCH = 'devs/set-search'
export const DEVS_SET_SORT_FIELD = 'devs/set-sort-field'

export const DEV_INIT_INPROGR = 'dev/init-inprogress'
export const DEV_INIT_SUCCESS = 'dev/init-success'
export const DEV_INIT_FAILURE = 'dev/init-failure'
export const DEV_RESET = 'dev/reset'
export const DEV_REFRESH_LOGS = 'dev/refresh-logs'
export const DEV_REFRESH_LOGS_INPROGR = 'dev/refresh-logs-inprogress'
export const DEV_REFRESH_LOGS_SUCCESS = 'dev/refresh-logs-success'
export const DEV_REFRESH_LOGS_FAILURE = 'dev/refresh-logs-failure'
export const DEV_PUBLISH_INPROGR = 'dev/publish-inprogress'
export const DEV_PUBLISH_SUCCESS = 'dev/publish-success'
export const DEV_PUBLISH_FAILURE = 'dev/publish-failure'
export const DEV_UNPUBLISH_INPROGR = 'dev/publish-inprogress'
export const DEV_UNPUBLISH_SUCCESS = 'dev/publish-success'
export const DEV_UNPUBLISH_FAILURE = 'dev/publish-failure'
export const DEV_DELETE_INPROGR = 'dev/delete-inprogress'
export const DEV_DELETE_SUCCESS = 'dev/delete-success'
export const DEV_DELETE_FAILURE = 'dev/delete-failure'
export const DEV_NAVIGATOR_SET_TAB = 'dev/navigator/set-tab'
export const DEV_SET_LOGS_FILTER = 'dev/set-logs-filter'

export const DEV_SET_META_INPROGR = 'dev/set-meta-inprogress'
export const DEV_SET_META_SUCCESS = 'dev/set-meta-success'
export const DEV_SET_META_FAILURE = 'dev/set-meta-failure'

export const DEV_FETCH_FILE_INPROGR = 'dev/fetch-file-inprogress'
export const DEV_FETCH_FILE_SUCCESS = 'dev/fetch-file-success'
export const DEV_FETCH_FILE_FAILURE = 'dev/fetch-file-failure'

export const DEV_CLAIM_SET_DEVICEID = 'dev/claim-set-deviceid'
export const DEV_CLAIM_SET_SECRET = 'dev/claim-set-secret'
export const DEV_CLAIM_INPROGR = 'dev/claim-inprogress'
export const DEV_CLAIM_SUCCESS = 'dev/claim-success'
export const DEV_CLAIM_FAILURE = 'dev/claim-failure'

export const DEV_SET_EDITING = 'dev/set-editing'

export const DEV_PATCH_INPROGR = 'dev/patch-inprogress'
export const DEV_PATCH_SUCCESS = 'dev/patch-success'
export const DEV_PATCH_FAILURE = 'dev/patch-failure'
