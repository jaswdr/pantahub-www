/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import * as Types from './types'

import {
  deleteDevice,
  getDevsData,
  publishDevice,
  unPublishDevice,
  claimDevice,
  setDeviceMetadata,
  fetchFile,
  patchDevice
} from '../../services/api'

import { logout } from '../auth/actions'
import { getAllDeviceData, getDeviceLogsService } from '../../services/devices.service'
import { resolvePath } from '../../lib/object.helpers'

const refreshTime = 60000 * 30 // 30 minutes

export const initializeDevicesInProgress = () => ({
  type: Types.DEVS_INIT_INPROGR
})

export const initializeDevicesSuccess = devices => ({
  type: Types.DEVS_INIT_SUCCESS,
  devices
})

export const initializeDevicesFailure = error => ({
  type: Types.DEVS_INIT_FAILURE,
  error
})

export const resetDevices = () => ({
  type: Types.DEVS_RESET
})

export const initializeDevices = token => async dispatch => {
  dispatch(initializeDevicesInProgress())

  try {
    const devs = await getDevsData(token)

    if (!devs.ok) {
      dispatch(initializeDevicesFailure(devs.json))
      dispatch(logout())
    } else {
      dispatch(initializeDevicesSuccess(devs.json))
    }
  } catch (err) {
    dispatch(initializeDevicesFailure({ code: 0, message: err }))
    throw err
  }
}

export const setDevicesSearch = search => ({
  type: Types.DEVS_SET_SEARCH,
  search
})

export const setDevicesSortField = field => ({
  type: Types.DEVS_SET_SORT_FIELD,
  field
})

// Single device actions
export const initializeDeviceInProgress = refresh => ({
  type: Types.DEV_INIT_INPROGR,
  refresh
})

export const initializeDeviceSuccess = (
  device
) => ({
  type: Types.DEV_INIT_SUCCESS,
  device
})

export const initializeDeviceFailure = error => ({
  type: Types.DEV_INIT_FAILURE,
  error
})

export const resetDevice = () => ({
  type: Types.DEV_RESET
})

export const initializeDevice = (
  token,
  timestamp,
  deviceId,
  revision,
  device,
  fromRefresher = false
) => async (dispatch, getState) => {
  const currentRevision = resolvePath(getState(), 'devs.current.device.history.currentStep.rev', undefined)
  revision = parseInt(revision, 10)
  const refresh =
    device === null ||
    device.id !== deviceId ||
    revision !== currentRevision ||
    timestamp === null ||
    timestamp < new Date(Date.now() - refreshTime)

  const args = {
    token,
    timestamp,
    deviceId,
    revision,
    device,
    fromRefresher,
    refresh,
    currentRevision
  }
  // mark as loading only if it's oneshot
  dispatch(initializeDeviceInProgress(refresh && !fromRefresher))
  try {
    const { consolidatedDevice } = await getAllDeviceData(args)
    dispatch(
      initializeDeviceSuccess(consolidatedDevice)
    )
  } catch (err) {
    dispatch(initializeDeviceFailure({ code: 0, message: err }))
    throw err
  }
  return false
}

export const deviceFetchNewLogsSuccess = (payload) => ({
  type: Types.DEV_REFRESH_LOGS_SUCCESS,
  payload
})

export const deviceFetchNewLogs = (
  token,
  deviceId,
  fromRefresher
) => async dispatch => {
  const response = await getDeviceLogsService(token, deviceId, fromRefresher)
  return dispatch(deviceFetchNewLogsSuccess(response))
}

export const deviceDeleteInProgress = id => ({
  type: Types.DEV_DELETE_INPROGR,
  id
})

export const deviceDeleteSuccess = id => ({
  type: Types.DEV_DELETE_SUCCESS,
  id
})

export const deviceDeleteFailure = error => ({
  type: Types.DEV_DELETE_FAILURE,
  error
})

export const deviceDelete = (token, deviceId) => async dispatch => {
  dispatch(deviceDeleteInProgress(deviceId))
  try {
    const resp = await deleteDevice(token, deviceId)

    if (!resp.ok) {
      dispatch(deviceDeleteFailure(resp.json))
      if (resp.status === 401) dispatch(logout())
    } else dispatch(deviceDeleteSuccess(deviceId))
  } catch (err) {
    dispatch(deviceDeleteFailure({ code: 0, message: err }))
    throw err
  }
}

export const devicePublishInProgress = id => ({
  type: Types.DEV_PUBLISH_INPROGR,
  id
})

export const devicePublishSuccess = (id, isPublic) => ({
  type: Types.DEV_PUBLISH_SUCCESS,
  id,
  isPublic
})

export const devicePublishFailure = error => ({
  type: Types.DEV_PUBLISH_FAILURE,
  error
})

export const devicePublish = (token, id, isPublic) => async dispatch => {
  dispatch(devicePublishInProgress(id))
  try {
    const resp = !isPublic
      ? await publishDevice(token, id)
      : await unPublishDevice(token, id)

    if (!resp.ok) {
      dispatch(devicePublishFailure(resp.json))
      if (resp.status === 401) dispatch(logout())
    } else dispatch(devicePublishSuccess(id, !isPublic))
  } catch (err) {
    dispatch(devicePublishFailure({ code: 0, message: err }))
    throw err
  }
}

export const deviceClaimSetDeviceID = id => ({
  type: Types.DEV_CLAIM_SET_DEVICEID,
  id
})

export const deviceClaimSetSecret = secret => ({
  type: Types.DEV_CLAIM_SET_SECRET,
  secret
})

export const deviceClaimInProgress = id => ({
  type: Types.DEV_CLAIM_INPROGR,
  id
})

export const deviceClaimSuccess = response => ({
  type: Types.DEV_CLAIM_SUCCESS,
  response
})

export const deviceClaimFailure = error => ({
  type: Types.DEV_CLAIM_FAILURE,
  error
})

export const deviceClaim = (token, id, secret) => async dispatch => {
  dispatch(deviceClaimInProgress(id))
  try {
    const resp = await claimDevice(token, id, secret)

    if (!resp.ok) {
      dispatch(deviceClaimFailure(resp.json))
      if (resp.status === 401) dispatch(logout())
    } else dispatch(deviceClaimSuccess(resp.json))
  } catch (err) {
    dispatch(deviceClaimFailure({ code: 0, message: err }))
  }
}

export const deviceNavigatorSetTab = tab => ({
  type: Types.DEV_NAVIGATOR_SET_TAB,
  tab
})

export const deviceSetLogsFilter = logsFilter => ({
  type: Types.DEV_SET_LOGS_FILTER,
  logsFilter
})

export const deviceSetMetaInProgress = id => ({
  type: Types.DEV_SET_META_INPROGR,
  id
})
export const deviceSetMetaSuccess = response => ({
  type: Types.DEV_SET_META_SUCCESS,
  response
})
export const deviceSetMetaFailure = error => ({
  type: Types.DEV_SET_META_FAILURE,
  error
})
export const deviceSetMeta = (token, id, meta, type) => async dispatch => {
  dispatch(deviceSetMetaInProgress(id))
  try {
    const resp = await setDeviceMetadata(token, id, meta, type)

    if (!resp.ok) {
      dispatch(deviceSetMetaFailure(resp.json))
      if (resp.status === 401) dispatch(logout())
    } else {
      dispatch(deviceSetMetaSuccess(resp.json))
    }
  } catch (err) {
    dispatch(deviceSetMetaFailure({ code: 0, message: err.message }))
  }
}

export const deviceFetchFileInProgress = (id, key) => ({
  type: Types.DEV_FETCH_FILE_INPROGR,
  id,
  key
})
export const deviceFetchFileSuccess = (id, key, content) => ({
  type: Types.DEV_FETCH_FILE_SUCCESS,
  id,
  key,
  content
})
export const deviceFetchFileFailure = (id, key, error) => ({
  type: Types.DEV_FETCH_FILE_FAILURE,
  id,
  key,
  error
})
export const deviceFetchFile = (token, id, url, key) => async dispatch => {
  dispatch(deviceFetchFileInProgress(url))
  try {
    const resp = await fetchFile(token, url)
    if (!resp.ok) {
      dispatch(deviceFetchFileFailure(id, key, resp))
      if (resp.status === 401) dispatch(logout())
    } else dispatch(deviceFetchFileSuccess(id, key, await resp.text()))
  } catch (err) {
    dispatch(deviceFetchFileFailure(id, key, { code: 0, error: err.message }))
  }
}

export const deviceSetEditing = editing => {
  return {
    type: Types.DEV_SET_EDITING,
    editing
  }
}

export const devicePatchInProgress = id => ({
  type: Types.DEV_PATCH_INPROGR,
  id
})

export const devicePatchSuccess = (id, response) => ({
  type: Types.DEV_PATCH_SUCCESS,
  id,
  response
})

export const devicePatchFailure = (id, error) => ({
  type: Types.DEV_PATCH_FAILURE,
  id,
  error
})

export const devicePatch = (token, id, payload) => async dispatch => {
  dispatch(devicePatchInProgress(id))
  try {
    const resp = await patchDevice(token, id, payload)

    if (!resp.ok) {
      dispatch(devicePatchFailure(resp.json))
      if (resp.status === 401) dispatch(logout())
    } else dispatch(devicePatchSuccess(id, resp.json))
  } catch (err) {
    dispatch(devicePatchFailure(id, { code: 0, message: err.message }))
  }
}
