/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import matches from 'lodash.matches'
import merge from 'lodash.merge'
import findIndex from 'lodash.findindex'

import * as Types from './types'
import { resolvePath } from '../../lib/object.helpers'

const devsInitialState = {
  devices: [],
  current: {
    device: null,
    rev: null,
    timestamp: null,
    navigator: {
      activeTab: 'readme',
      setMeta: {
        loading: false,
        response: null,
        error: null
      },
      logsFilter: null,
      files: {}
    },
    editing: false
  },
  logs: {
    newestLogEntryTime: null,
    entries: []
  },
  timestamp: null,
  error: null,
  loading: false,
  refreshing: false,
  search: '',
  sortField: 'timestamp',
  sortDirection: null,
  delete: {},
  publish: {},
  unpublish: {},
  claim: {
    loading: false,
    response: null,
    error: null,
    device: null,
    secret: null
  },
  initializing: false
}

function mergeLogs (state, newLogs) {
  return [
    ...resolvePath(state, 'logs.entries', []),
    ...newLogs
  ]
    .slice(-(parseInt(process.env.REACT_APP_LOGS_SCROLLBACK) || 5000))
    .filter(e =>
      state.current.logsFilter !== null
        ? matches(state.current.logsFilter)(e)
        : true
    )
}

export const devsReducer = (state = devsInitialState, action) => {
  switch (action.type) {
    case Types.DEVS_INIT_INPROGR:
    case Types.DEV_INIT_INPROGR:
      return {
        ...state,
        initializing: true,
        loading: action.refresh !== undefined ? action.refresh : true,
        refreshing: action.refresh,
        search: action.refresh ? state.search : ''
      }

    case Types.DEVS_INIT_SUCCESS:
      return {
        ...devsInitialState,
        initializing: false,
        devices: action.devices,
        timestamp: new Date()
      }

    case Types.DEVS_INIT_FAILURE:
    case Types.DEV_INIT_FAILURE:
      return {
        ...state,
        initializing: false,
        error: action.error,
        loading: false
      }

    case Types.DEVS_SET_SEARCH:
      return {
        ...state,
        search: action.search
      }

    case Types.DEVS_SET_SORT_FIELD:
      if (state.loading) return state
      const sortDirection =
        action.field === state.sortField ? !state.sortDirection : true
      return {
        ...state,
        sortField: action.field,
        sortDirection
      }

    case Types.DEVS_RESET:
    case Types.DEV_RESET:
      return { ...devsInitialState }

    case Types.DEV_REFRESH_LOGS_SUCCESS:
      return {
        ...state,
        logs: {
          newestLogEntryTime: action.payload.newestLogEntryTime,
          entries: mergeLogs(state, action.payload.newLogs)
        }
      }

    case Types.DEV_INIT_SUCCESS:
      return {
        ...state,
        loading: false,
        initializing: false,
        current: {
          ...state.current,
          device: {
            ...((state.current || {}).device || {}),
            ...(action.device || {})
          },
          timestamp: new Date()
        }
      }

    case Types.DEV_SET_LOGS_FILTER:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            logsFilter: action.logsFilter
          }
        }
      }

    case Types.DEV_NAVIGATOR_SET_TAB:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            activeTab: action.tab
          }
        }
      }

    case Types.DEV_DELETE_INPROGR:
      return merge({}, state, {
        delete: {
          [action.id]: {
            loading: true
          }
        }
      })
    case Types.DEV_DELETE_SUCCESS:
      // delete the device from store to avoid the roundtrip
      return merge({}, state, {
        devices: state.devices.filter(d => d.deviceid !== action.id),
        delete: {
          [action.id]: {
            loading: false
          }
        }
      })
    case Types.DEV_DELETE_FAILURE:
      return merge({}, state, {
        delete: {
          [action.id]: {
            loading: false,
            error: action.error
          }
        }
      })

    case Types.DEV_PUBLISH_INPROGR:
      return merge({}, state, {
        publish: {
          [action.id]: {
            loading: true
          }
        }
      })
    case Types.DEV_PUBLISH_SUCCESS:
      const devIndex = findIndex(state.devices, { deviceid: action.id })
      const allDevs = state.devices.slice()
      const dev = allDevs[devIndex]
      allDevs[devIndex] = {
        ...dev,
        public: action.isPublic
      }

      return {
        ...state,
        devices: allDevs,
        publish: {
          ...state.publish,
          [action.id]: {
            loading: false
          }
        }
      }
    case Types.DEV_PUBLISH_FAILURE:
      return merge({}, state, {
        publish: {
          [action.id]: {
            loading: false,
            error: action.error
          }
        }
      })

    case Types.DEV_CLAIM_SET_DEVICEID:
      return merge({}, state, {
        claim: {
          device: action.id
        }
      })
    case Types.DEV_CLAIM_SET_SECRET:
      return merge({}, state, {
        claim: {
          secret: action.secret
        }
      })
    case Types.DEV_CLAIM_INPROGR:
      return merge({}, state, {
        claim: {
          loading: true
        }
      })
    case Types.DEV_CLAIM_SUCCESS:
      return merge({}, state, {
        claim: {
          loading: false,
          response: action.response,
          error: null,
          device: null,
          secret: null
        }
      })
    case Types.DEV_CLAIM_FAILURE:
      return merge({}, state, {
        claim: {
          loading: false,
          error: action.error,
          response: null,
          device: null,
          secret: null
        }
      })
    case Types.DEV_SET_META_INPROGR:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            setMeta: {
              loading: true
            }
          }
        }
      }
    case Types.DEV_SET_META_SUCCESS:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            setMeta: {
              loading: false
            }
          }
        }
      }
    case Types.DEV_SET_META_FAILURE:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            setMeta: {
              loading: false,
              error: action.error
            }
          }
        }
      }

    case Types.DEV_FETCH_FILE_INPROGR:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            files: {
              ...state.current.navigator.files,
              [action.id]: {
                ...(state.current.navigator.files[action.id] || {}),
                [action.key]: { loading: true }
              }
            }
          }
        }
      }
    case Types.DEV_FETCH_FILE_FAILURE:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            files: {
              ...state.current.navigator.files,
              [action.id]: {
                ...(state.current.navigator.files[action.id] || {}),
                [action.key]: { loading: false, error: action.error }
              }
            }
          }
        }
      }
    case Types.DEV_FETCH_FILE_SUCCESS:
      return {
        ...state,
        current: {
          ...state.current,
          navigator: {
            ...state.current.navigator,
            files: {
              ...state.current.navigator.files,
              [action.id]: {
                ...(state.current.navigator.files[action.id] || {}),
                [action.key]: { loading: false, content: action.content }
              }
            }
          }
        }
      }
    case Types.DEV_SET_EDITING:
      return {
        ...state,
        current: {
          ...state.current,
          editing: action.editing
        }
      }
    case Types.DEV_PATCH_INPROGR:
      return {
        ...state,
        saving: {
          ...state.saving,
          [action.id]: 'inprogr'
        }
      }
    case Types.DEV_PATCH_SUCCESS:
      return {
        ...state,
        saving: {
          ...state.saving,
          [action.id]: 'success'
        }
      }
    case Types.DEV_PATCH_FAILURE:
      return {
        ...state,
        saving: {
          ...state.saving,
          [action.id]: 'failure'
        }
      }

    default:
      return state
  }
}
