import * as Types from '../types'
import * as Actions from '../actions'
import createStore from '../../index'
import * as deviceMocks from '../../../mocks/devices'
import * as SummaryMocks from '../../../mocks/device_summary'
import * as StepsMocks from '../../../mocks/trails_steps'
import * as ObjectMocks from '../../../mocks/objects'
import { buildCurrentStep } from '../../../services/devices.service'

const TOKEN = 'Testing Token'
const refreshTime = 90000 * 30

jest.mock('../../../services/api.js')

describe('Devices Actions', () => {
  describe('initializeDevice', () => {
    const store = createStore()
    const getState = store.getState

    it('start initialize for first time', async () => {
      const dispatch = jest.fn()
      await Actions.initializeDevice(TOKEN, null, deviceMocks.device.id, null, null, false)(dispatch, getState)

      expect(dispatch.mock.calls.length).toBe(2)
      expect(dispatch.mock.calls[0][0]).toEqual({ 'refresh': true, 'type': 'dev/init-inprogress' })
      expect(dispatch.mock.calls[1][0].type).toEqual(Types.DEV_INIT_SUCCESS)
      expect(dispatch.mock.calls[1][0].device.id).toEqual(deviceMocks.device.id)
      expect(dispatch.mock.calls[1][0].device.summary).toEqual(SummaryMocks.summary)
      expect(dispatch.mock.calls[1][0].device.history.revision).toEqual(16)
      expect(dispatch.mock.calls[1][0].device.history.lastRevision).toEqual(16)
      expect(dispatch.mock.calls[1][0].device.history.revIndex).toEqual(16)
      expect(dispatch.mock.calls[1][0].device.history.currentStep.rawState)
        .toEqual(StepsMocks.steps.find(s => s.rev === 16).state)
      expect(dispatch.mock.calls[1][0].device.history.currentStep.state)
        .toEqual(buildCurrentStep(StepsMocks.steps, ObjectMocks.objects, 16).state)
    })

    it('refresh element data', async () => {
      const dispatch = jest.fn()
      await store.dispatch(Actions.initializeDevice(TOKEN, null, deviceMocks.device.id, null, null, false))
      let revision = 16
      await Actions.initializeDevice(
        TOKEN,
        new Date(Date.now() - refreshTime),
        deviceMocks.device.id,
        revision,
        deviceMocks.device,
        true
      )(dispatch, getState)

      expect(dispatch.mock.calls.length).toBe(2)
      expect(dispatch.mock.calls[0][0]).toEqual({ 'refresh': false, 'type': 'dev/init-inprogress' })
      expect(dispatch.mock.calls[1][0].type).toEqual(Types.DEV_INIT_SUCCESS)
      expect(dispatch.mock.calls[1][0].device.id).toEqual(deviceMocks.device.id)
      expect(dispatch.mock.calls[1][0].device.summary).toEqual(SummaryMocks.summary)
      expect(dispatch.mock.calls[1][0].device.history.revision).toEqual(revision)
      expect(dispatch.mock.calls[1][0].device.history.lastRevision).toEqual(16)
      expect(dispatch.mock.calls[1][0].device.history.revIndex).toEqual(revision)
      expect(dispatch.mock.calls[1][0].device.history.currentStep.rawState)
        .toEqual(StepsMocks.steps.find(s => s.rev === revision).state)
      expect(dispatch.mock.calls[1][0].device.history.currentStep.state)
        .toEqual(buildCurrentStep(StepsMocks.steps, ObjectMocks.objects, revision).state)

      revision = 10
      await Actions.initializeDevice(
        TOKEN,
        new Date(Date.now() - refreshTime),
        deviceMocks.device.id,
        revision,
        deviceMocks.device,
        true
      )(dispatch, getState)

      expect(dispatch.mock.calls.length).toBe(4)
      expect(dispatch.mock.calls[3][0].type).toEqual(Types.DEV_INIT_SUCCESS)
      expect(dispatch.mock.calls[3][0].device.id).toEqual(deviceMocks.device.id)
      expect(dispatch.mock.calls[3][0].device.summary).toEqual(SummaryMocks.summary)
      expect(dispatch.mock.calls[3][0].device.history.revision).toEqual(revision)
      expect(dispatch.mock.calls[3][0].device.history.lastRevision).toEqual(16)
      expect(dispatch.mock.calls[3][0].device.history.revIndex).toEqual(revision)
      expect(dispatch.mock.calls[3][0].device.history.currentStep.rawState)
        .toEqual(StepsMocks.steps.find(s => s.rev === revision).state)
      expect(dispatch.mock.calls[3][0].device.history.currentStep.state)
        .toEqual(buildCurrentStep(StepsMocks.steps, ObjectMocks.objects, revision).state)
    })
  })
})
