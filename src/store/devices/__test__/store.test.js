import * as Actions from '../actions'
import createStore from '../../index'
import * as deviceMocks from '../../../mocks/devices'
import * as SummaryMocks from '../../../mocks/device_summary'
import * as StepsMocks from '../../../mocks/trails_steps'
import * as ObjectMocks from '../../../mocks/objects'
import { buildCurrentStep } from '../../../services/devices.service'

const TOKEN = 'Testing Token'
const refreshTime = 90000 * 30

jest.mock('../../../services/api.js')

describe('Devices Store', () => {
  describe('initializeDevice', () => {
    const store = createStore()

    it('start initialize for first time', async () => {
      await store.dispatch(Actions.initializeDevice(TOKEN, null, deviceMocks.device.id, null, null, false))
      const state = store.getState()
      expect(state.devs.current.device.id).toEqual(deviceMocks.device.id)
      expect(state.devs.current.device.summary).toEqual(SummaryMocks.summary)
      expect(state.devs.current.device.history.revision).toEqual(16)
      expect(state.devs.current.device.history.lastRevision).toEqual(16)
      expect(state.devs.current.device.history.revIndex).toEqual(16)
      expect(state.devs.current.device.history.currentStep.rawState)
        .toEqual(StepsMocks.steps.find(s => s.rev === 16).state)
      expect(state.devs.current.device.history.currentStep.state)
        .toEqual(buildCurrentStep(StepsMocks.steps, ObjectMocks.objects, 16).state)
    })

    it('refresh element data', async () => {
      let revision = 16
      await store.dispatch(Actions.initializeDevice(
        TOKEN,
        new Date(Date.now() - refreshTime),
        deviceMocks.device.id,
        revision,
        deviceMocks.device,
        true
      ))
      let state = store.getState()

      expect(state.devs.current.device.id).toEqual(deviceMocks.device.id)
      expect(state.devs.current.device.summary).toEqual(SummaryMocks.summary)
      expect(state.devs.current.device.history.revision).toEqual(16)
      expect(state.devs.current.device.history.lastRevision).toEqual(16)
      expect(state.devs.current.device.history.revIndex).toEqual(16)
      expect(state.devs.current.device.history.currentStep.rawState)
        .toEqual(StepsMocks.steps.find(s => s.rev === 16).state)
      expect(state.devs.current.device.history.currentStep.state)
        .toEqual(buildCurrentStep(StepsMocks.steps, ObjectMocks.objects, 16).state)

      revision = 10
      await store.dispatch(Actions.initializeDevice(
        TOKEN,
        new Date(Date.now() - refreshTime),
        deviceMocks.device.id,
        revision,
        deviceMocks.device,
        true
      ))

      state = store.getState()
      expect(state.devs.current.device.id).toEqual(deviceMocks.device.id)
      expect(state.devs.current.device.summary).toEqual(SummaryMocks.summary)
      expect(state.devs.current.device.history.revision).toEqual(revision)
      expect(state.devs.current.device.history.lastRevision).toEqual(16)
      expect(state.devs.current.device.history.revIndex).toEqual(revision)
      expect(state.devs.current.device.history.currentStep.rawState)
        .toEqual(StepsMocks.steps.find(s => s.rev === revision).state)
      expect(state.devs.current.device.history.currentStep.state)
        .toEqual(buildCurrentStep(StepsMocks.steps, ObjectMocks.objects, revision).state)
    })
  })
})
