import * as Types from './types'

const initialErrors = {
  type: null,
  error: null
}

export function generalErrors (state = initialErrors, action) {
  switch (action.type) {
    case Types.GENERAL_ERROR_CLEAN:
      return initialErrors
    case Types.GENERAL_ERROR_SET:
      return {
        ...state,
        ...action.payload
      }
    default:
      return state
  }
}
