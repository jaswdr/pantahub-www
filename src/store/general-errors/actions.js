import * as Types from './types'

export function setError (type, error) {
  return {
    type: Types.GENERAL_ERROR_SET,
    payload: {
      type,
      error
    }
  }
}

export function clearError () {
  return {
    type: Types.GENERAL_ERROR_CLEAN
  }
}
