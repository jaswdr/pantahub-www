/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/* global localStorage */
import { push } from 'connected-react-router'

import * as Types from './types'

import { login, register, verifyAccount, getUserData, postPassword, requestPasswordRecover } from '../../services/api'
import { ErrorIfNotAuthorized, parseImplicitData } from '../../lib/api.helpers'

import { tokenKey } from '../../lib/auth'

import { resetDashboard } from '../dashboard/actions'
import { resetDevices } from '../devices/actions'
import { loginPath } from '../../router/routes'

export const setUsername = username => ({
  type: Types.AUTH_SET_USERNAME,
  username
})

export const setUserData = (payload) => ({
  type: Types.AUTH_SET_USER_DATA,
  payload
})

export const setEmail = email => ({
  type: Types.AUTH_SET_EMAIL,
  email
})

export const setPassword = password => ({
  type: Types.AUTH_SET_PASSWORD,
  password
})

export const getTokenInProgress = () => ({
  type: Types.AUTH_GET_TOKEN_INPROGR
})

export const getTokenSuccess = token => ({
  type: Types.AUTH_GET_TOKEN_SUCCESS,
  token
})

export const getTokenError = error => ({
  type: Types.AUTH_GET_TOKEN_FAILURE,
  error
})

export const fetchUserData = () => async (dispatch, getState) => {
  const response = await getUserData(getState().auth.token)
  ErrorIfNotAuthorized(response)

  return dispatch(setUserData(response.json))
}

export const getToken = (username, password) => async dispatch => {
  dispatch(getTokenInProgress())

  try {
    const resp = await login(username, password)
    if (!resp.ok) dispatch(getTokenError(resp.json))
    else {
      const { token } = resp.json
      localStorage.setItem(tokenKey, `${token}|${username}`)
      dispatch(getTokenSuccess(token))
    }
  } catch (err) {
    dispatch(
      getTokenError({
        code: err.code || 0,
        message: err.message || JSON.stringify(err)
      })
    )
  }
}

export const removeToken = () => ({
  type: Types.AUTH_REMOVE_TOKEN
})

export const logout = () => {
  return async dispatch => {
    await localStorage.removeItem(tokenKey)
    dispatch(removeToken())
    dispatch(resetDashboard())
    return dispatch(resetDevices())
  }
}

export const signUpInProgress = () => ({
  type: Types.AUTH_SIGNUP_INPROGR
})

export const signUpSuccess = response => ({
  type: Types.AUTH_SIGNUP_SUCCESS,
  response
})

export const signUpFailure = error => ({
  type: Types.AUTH_SIGNUP_FAILURE,
  error
})

export const signUp = (nick, email, password, captchaToken, encrypted) => async dispatch => {
  dispatch(signUpInProgress())
  try {
    const resp = await register(nick, email, password, captchaToken, encrypted)
    if (!resp.ok) dispatch(signUpFailure(resp.json))
    else dispatch(signUpSuccess(resp.json))
  } catch (err) {
    dispatch(signUpFailure(err.message))
  }
}

export const verificationInProgress = () => ({
  type: Types.AUTH_VERIFY_INPROGR
})

export const verificationSuccess = response => ({
  type: Types.AUTH_VERIFY_SUCCESS,
  response
})

export const verificationFailure = error => ({
  type: Types.AUTH_VERIFY_FAILURE,
  error
})

export const verify = (id, challenge) => async dispatch => {
  dispatch(verificationInProgress())
  try {
    const resp = await verifyAccount(id, challenge)
    if (!resp.ok) dispatch(verificationFailure(resp.json))
    else dispatch(verificationSuccess(resp.json))
  } catch (err) {
    dispatch(verificationFailure({ code: 0, message: err }))
  }
}

export const requestPasswordEmailInProgress = () => ({
  type: Types.AUTH_REQUEST_PASSWORD_PROGRESS
})

export const requestPasswordEmailSuccess = () => ({
  type: Types.AUTH_REQUEST_PASSWORD_SUCCESS
})

export const requestPasswordEmailFailure = (error = '') => ({
  type: Types.AUTH_REQUEST_PASSWORD_FAILURE,
  error: error.replace(/\(sid:.*\)/, '')
})

export const resetErrorOnPasswordEmail = () => ({
  type: Types.AUTH_REQUEST_PASSWORD_FAILURE,
  error: null
})

export const requestPasswordEmail = (email) => async dispatch => {
  dispatch(requestPasswordEmailInProgress())
  try {
    const resp = await requestPasswordRecover(email)
    if (!resp.ok) dispatch(requestPasswordEmailFailure(resp.json.Error))
    else dispatch(requestPasswordEmailSuccess())
  } catch (err) {
    dispatch(requestPasswordEmailFailure({ code: 0, message: err }))
  }
}

export const setNewPasswordInProgress = () => ({
  type: Types.AUTH_SET_NEW_PASSWORD_PROGRESS
})

export const setNewPasswordSuccess = () => ({
  type: Types.AUTH_SET_NEW_PASSWORD_SUCCESS
})

export const setNewPasswordFailure = (error = '') => ({
  type: Types.AUTH_SET_NEW_PASSWORD_FAILURE,
  error: error.replace(/\(sid:.*\)/, '')
})

export const setNewPassword = (token, password) => async dispatch => {
  dispatch(setNewPasswordInProgress())
  try {
    const resp = await postPassword(token, password)
    if (!resp.ok) return dispatch(setNewPasswordFailure(resp.json.Error))

    dispatch(setNewPasswordSuccess(resp.json))
    dispatch(push(loginPath))
  } catch (err) {
    dispatch(setNewPasswordFailure({ code: 0, message: err }))
  }
}

export const setEmailTokenFromHash = (hash = '') => ({
  type: Types.AUTH_SET_EMAIL_TOKEN,
  token: parseImplicitData(hash).token
})
