/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

export const AUTH_SET_USERNAME = 'auth/set-username'
export const AUTH_SET_EMAIL = 'auth/set-email'
export const AUTH_SET_PASSWORD = 'auth/set-password'
export const AUTH_GET_TOKEN_INPROGR = 'auth/get-token-inprogress'
export const AUTH_GET_TOKEN_SUCCESS = 'auth/get-token-success'
export const AUTH_GET_TOKEN_FAILURE = 'auth/get-token-failure'
export const AUTH_REMOVE_TOKEN = 'auth/remove-token'

export const AUTH_SIGNUP_INPROGR = 'auth/signup-inprogress'
export const AUTH_SIGNUP_SUCCESS = 'auth/signup-success'
export const AUTH_SIGNUP_FAILURE = 'auth/signup-failure'

export const AUTH_VERIFY_INPROGR = 'auth/verify-inprogress'
export const AUTH_VERIFY_SUCCESS = 'auth/verify-success'
export const AUTH_VERIFY_FAILURE = 'auth/verify-failure'
export const AUTH_SET_USER_DATA = 'auth/set-user-data'

export const AUTH_REQUEST_PASSWORD = 'auth/request-new-password'
export const AUTH_REQUEST_PASSWORD_PROGRESS = 'auth/request-new-password/progress'
export const AUTH_REQUEST_PASSWORD_SUCCESS = 'auth/request-new-password/success'
export const AUTH_REQUEST_PASSWORD_FAILURE = 'auth/request-new-password/failure'

export const AUTH_SET_NEW_PASSWORD = 'auth/save-new-password'
export const AUTH_SET_NEW_PASSWORD_PROGRESS = 'auth/save-new-password/progress'
export const AUTH_SET_NEW_PASSWORD_SUCCESS = 'auth/save-new-password/success'
export const AUTH_SET_NEW_PASSWORD_FAILURE = 'auth/save-new-password/failure'

export const AUTH_SET_EMAIL_TOKEN = 'auth/set-email-token'
