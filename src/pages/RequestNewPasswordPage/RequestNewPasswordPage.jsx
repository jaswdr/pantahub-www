import React from 'react'
import RequestNewPassword from '../../components/PasswordRecovery/RequestNewPassword'

export default function RequestNewPasswordPage () {
  return (
    <section className="container">
      <section className="rnp-page col-sm-12 d-flex flex-column justify-content-center align-items-center">
        <header className="rnp-page__header col-sm-6 text-center">
          <h1>Password Reset</h1>
          <p>
            Enter the <b>email address</b> that you used to register.
            <br/>
            {"We'll send you an email with a link to reset your password."}
          </p>
        </header>
        <RequestNewPassword />
      </section>
    </section>
  )
}
