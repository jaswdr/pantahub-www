/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'

import connectYourDeviceImage from '../../assets/images/connect-your-devices.png'
import testAndDeployImage from '../../assets/images/test-and-deploy.png'
import orgImage from '../../assets/images/org.png'
import funnelImage from '../../assets/images/funnel.png'
import './about_us.scss'

export default class AboutUsPage extends Component {
  render () {
    return (
      <div className="about-us landing-page container-fluid">
        <div className="row hero">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-12 welcome">
                <header>
                  <h1 className="text-center">Enabling Edge Services for Linux devices</h1>
                </header><center>
                  <p>
                    <br/>Pantacor brings their open source skills to software and firmware management with Pantahub, a community ecosystem driven software delivery and device management platform.<br/><br/> By turning fixed function devices into software defined infrastructure, from small deployments to large-scale enterprise fleets, we enable horizontal extensibility in a supportable and efficient way.
                  </p>
                </center>
              </div>
            </div>
          </div>
        </div>
        <div className="features text-center">
          <div className="container pb-30">
            <div className="row justify-content-center">
              <header className="col-12">
                <h1 className="section-title">Mission</h1>
              </header>
              <div className="col-md-6 pt-20">
                <p>
                  The team at Pantacor is committed to turning the Intelligent Edge vision into reality. With hundred of millions of WiFi routers, network edge CPEs and all kinds of connected devices, the Intelligent Edge is almost there.<br/><br/>Giving existing Linux-based device deployments a second life through new value-add services, and a comprehensive management and deployment story through open source technologies, is the key to this new computing revolution.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="about-us sell text-center">
          <div className="container pb-30">
            <div className="row justify-content-center">
              <header className="col-12">
                <h1 className="section-title">Products</h1>
              </header>
            </div>
            <div className="row justify-content-center pt-40">
              <div className="col col-6 text-center justify-content-center">
                <div
                  style={{ backgroundImage: `url(${connectYourDeviceImage})` }}
                  className="feature-image feature-image--orange"
                />
                <div className="feature-title-row mb-10 mt-20">
                  <h4><b>Pantahub</b></h4>
                </div>
                <div className="feature-description-row">
                  <p>
                    Pantahub offers cloud controlled firmware and software management for all kinds of Linux based connected devices. In addition to this, Pantahub provides a community ecosystem for device makers, operators and technology enthusiasts to gather around and share their work.
                  </p>
                </div>
              </div>
              <div className="col col-6 text-center justify-content-center">
                <div
                  style={{ backgroundImage: `url(${testAndDeployImage})` }}
                  className="feature-image feature-image--orange"
                />
                <div className="feature-title-row mb-10 mt-20">
                  <h4><b>Pantavisor</b></h4>
                </div>
                <div className="feature-description-row">
                  <p>
                    Pantavisor is the Linux device init system that turns all the runtime into a set of container micro functions. Through Pantavisor, devices become software defined infrastructure, through Linux container technology. All Pantavisor enabled devices can automatically connect to a Pantahub instance.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="features text-center">
          <div className="container pb-30">
            <div className="row justify-content-center">
              <header className="col-12">
                <h1 className="section-title">Markets and Ecosystem</h1>
              </header>
              <div className="row justify-content-center pt-4 pb-4">
                <div className="col col-4 text-center justify-content-center">
                  <div
                    className="feature-image"
                  >
                    <i className="mdi mdi-wifi" aria-hidden="true" />
                  </div>
                  <div className="feature-title-row mb-10 mt-20">
                    <h4><b>Domestic and Commercial WiFi</b></h4>
                    <br/>
                  </div>
                  <div className="feature-description-row">
                    <p>
                      WiFi routers and CPEs already make up the largest Edge connectivity real estate in the world. Turning these into software-defined devices and enabling value-add applications and services on them means a constant monetization pipeline with near zero investment.
                    </p>
                  </div>
                </div>
                <div className="col col-4 text-center justify-content-center">
                  <div
                    style={{ backgroundImage: `url(${orgImage})` }}
                    className="feature-image"
                  />
                  <div className="feature-title-row mb-10 mt-20">
                    <h4><b>Enterprise networks</b></h4>
                    <br/>
                  </div>
                  <div className="feature-description-row">
                    <p>
                      On premise networking equipment becomes more powerful every day. With cloud computing doing the heavy lifting, the final mile has become increasingly more important to accelerate business logic. Extending the cloud into enterprise premises with managed Edge technology is the future of enterprise clouds.
                    </p>
                  </div>
                </div>
                <div className="col col-4 text-center justify-content-center">
                  <div
                    style={{ backgroundImage: `url(${funnelImage})` }}
                    className="feature-image"
                  />
                  <div className="feature-title-row mb-10 mt-20">
                    <h4><b>Telecom operators</b></h4>
                    <br/>
                  </div>
                  <div className="feature-description-row">
                    <p>
                      The race to squeeze the last dime out of pure network infrastructure is in the past, with the move to 5G and future technologies it is all about monetizing the use of the network. Leveraging your network to get Edge Services as close to the user as possible is key to this goal.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row contact text-center" id="contact_us">
          <div className="container">
            <div className="row">
              <header className="col-12">
                <h2>Contact Us</h2>
              </header>
              <div className="col-12">
                <div className="row">
                  <div className="col-4 col-md-5" />
                  <div className="col-4 col-md-2">
                    <hr />
                  </div>
                  <div className="col-4 col-md-5" />
                </div>
              </div>
              <div className="col-12">
                <div className="row">
                  <div className="col-2 col-md-4" />
                  <p className="col-8 col-md-4">
                    Questions, feedback or want to explore how our technology can help?
                    We love to hear from you, so just shoot us a mail.<br/><br/>
                    <a href="mailto:team@pantahub.com">team@pantahub.com</a>
                  </p>
                  <div className="col-2 col-md-4" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
