import React, { useEffect, useState } from 'react'
import CreateNewPassword from '../../components/PasswordRecovery/CreateNewPassword'
import { parseImplicitData } from '../../lib/api.helpers'

export default function ResetPasswordPage ({ location }) {
  const [token, setToken] = useState()

  useEffect(() => {
    setToken(parseImplicitData(location.hash).token)
  }, [])

  return (
    <section className="container">
      <section className="rpp-page col-sm-12 d-flex flex-column justify-content-center align-items-center">
        <header className="rpp-page__header col-sm-6 text-center">
          <h1>Set you new password</h1>
        </header>
        <CreateNewPassword token={token} />
      </section>
    </section>
  )
}
