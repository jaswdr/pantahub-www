/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Link } from 'react-router-dom'

import connectYourDeviceImage from '../../assets/images/connect-your-devices.png'
import manageYourCodeImage from '../../assets/images/manage-your-code.png'
import testAndDeployImage from '../../assets/images/test-and-deploy.png'
import orgImage from '../../assets/images/org.png'
import peopleImage from '../../assets/images/people.png'
import funnelImage from '../../assets/images/funnel.png'

import SignUpForm from '../../components/SignUpForm/SignUpForm'

import { logout } from '../../store/auth/actions'

import { userDashboardPath, loginPath } from '../../router/routes'
import { withAuthenticatedEvent } from '../../store/ga-middleware'

class LandingPage extends Component {
  render () {
    const { token, username, onLogout } = this.props
    return (
      <div className="landing-page container-fluid">
        <div className="row hero">
          <div className="container">
            <div className="row justify-content-between">
              <div className="col-md-6 welcome">
                <div className="row">
                  <header className="col-12">
                    <h1>Welcome to PantaHub</h1>
                  </header>
                  <p className="col-12">
                    The only place where Linux firmware can be <strong>shared and
              deployed</strong> for any device.
                  </p>
                </div>
                <div className="row welcome-images justify-content-around">
                  <div className="welcome-images-item">
                    <div
                      className="welcome-image"
                      style={{
                        backgroundImage: `url(${connectYourDeviceImage})`
                      }}
                    />
                    <div className="welcome-image-text">
                      Connect your Devices
                    </div>
                  </div>
                  <div className="welcome-images-item">
                    <div
                      className="welcome-image"
                      style={{
                        backgroundImage: `url(${manageYourCodeImage})`
                      }}
                    />
                    <div className="welcome-image-text">Deploy Software</div>
                  </div>
                  <div className="welcome-images-item">
                    <div
                      className="welcome-image"
                      style={{
                        backgroundImage: `url(${testAndDeployImage})`
                      }}
                    />
                    <div className="welcome-image-text">Share Firmwares</div>
                  </div>
                </div>
              </div>
              <div className="col-md-5 signup">
                {token ? (
                  <React.Fragment>
                    <h3>Start using PantaHub</h3>
                    <Link
                      to={`${userDashboardPath}/${username}`}
                      className="btn btn-dark btn-block"
                    >
                        Dashboard
                    </Link>
                    <Link
                      to={`${userDashboardPath}/${username}/devices`}
                      className="btn btn-dark btn-block"
                    >
                        Your Devices
                    </Link>
                    <a href="#contact_us" className="btn btn-dark btn-block">
                        Contact Us
                    </a>
                    <button onClick={onLogout} className="btn btn-dark btn-block">
                        Log Out
                    </button>
                  </React.Fragment>)
                  : (
                    <React.Fragment>
                      <h3>Register Now</h3>
                      <SignUpForm />
                      <p>
                            Already in PantaHub? <Link to={loginPath}>Log In</Link>
                      </p>
                    </React.Fragment>
                  )
                }
              </div>
            </div>
          </div>
        </div>
        <div className="row sell text-center">
          <div className="container">
            <div className="row">
              <header className="col-12">
                <h2>A Hub for all your Linux devices</h2>
              </header>
              <div className="col-12">
                <div className="row">
                  <div className="col-4 col-md-5" />
                  <div className="col-4 col-md-2">
                    <hr />
                  </div>
                  <div className="col-4 col-md-2" />
                </div>
              </div>
              <div className="col-12">
                <div className="row">
                  <div className="col-1 col-md-2" />
                  <p className="col-10 col-md-8">
                    Thanks to our Pantavisor device agent and through the use of container technology, Pantahub lets you connect your devices and transform them into bare-metal infrastructure. Designed for deeply embedded and high-grade enterprise systems alike, Pantavisor and Pantahub are the perfect choice for the management of your Linux devices and their firmware lifecycles.
                  </p>
                  <div className="col-1 col-md-2" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row features">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <div className="row feature text-center">
                  <div className="col-12">
                    <div className="row">
                      <div className="col-1 col-md-3" />
                      <div className="col-10 col-md-6 feature-image-row">
                        <div className="row justify-content-center">
                          <div
                            style={{ backgroundImage: `url(${orgImage})` }}
                            className="feature-image"
                          />
                        </div>
                      </div>
                      <div className="col-1 col-md-3" />
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="row">
                      <div className="col-1 col-md-3" />
                      <div className="col-10 col-md-6 feature-title-row">
                        <h4>Containers for the embedded Linux ecosystem</h4>
                      </div>
                      <div className="col-1 col-md-3" />
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="row">
                      <div className="col-1 col-md-3" />
                      <div className="col-10 col-md-6 feature-description-row">
                        <p>
      We use container technology to make the development, deployment and management of firmware on Linux devices as simple as it has ever been. From tiny OpenWRT routers to carrier-grade base stations, Pantahub and Pantavisor are the right choice.
                        </p>
                      </div>
                      <div className="col-1 col-md-3" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12">
                <div className="row feature text-center">
                  <div className="col-12">
                    <div className="row">
                      <div className="col-1 col-md-3" />
                      <div className="col-10 col-md-6 feature-image-row">
                        <div className="row justify-content-center">
                          <div
                            style={{ backgroundImage: `url(${peopleImage})` }}
                            className="feature-image"
                          />
                        </div>
                      </div>
                      <div className="col-1 col-md-3" />
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="row">
                      <div className="col-1 col-md-3" />
                      <div className="col-10 col-md-6 feature-title-row">
                        <h4>Share and Collaborate</h4>
                      </div>
                      <div className="col-1 col-md-3" />
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="row">
                      <div className="col-1 col-md-3" />
                      <div className="col-10 col-md-6 feature-description-row">
                        <p>
                          Share your firmware and projects as simple as with a git tree:<br/>
                          <code>
                            pvr clone; pvr commit; pvr post
                          </code>
                        </p>
                      </div>
                      <div className="col-1 col-md-3" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12">
                <div className="row feature text-center">
                  <div className="col-12">
                    <div className="row">
                      <div className="col-1 col-md-3" />
                      <div className="col-10 col-md-6 feature-image-row">
                        <div className="row justify-content-center">
                          <div
                            style={{ backgroundImage: `url(${funnelImage})` }}
                            className="feature-image"
                          />
                        </div>
                      </div>
                      <div className="col-1 col-md-3" />
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="row">
                      <div className="col-1 col-md-3" />
                      <div className="col-10 col-md-6 feature-title-row">
                        <h4>Fully Open</h4>
                      </div>
                      <div className="col-1 col-md-3" />
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="row">
                      <div className="col-1 col-md-3" />
                      <div className="col-10 col-md-6 feature-description-row">
                        <p>
      Since day one Pantacor has been committed to a fully open source model. Feel free to use and improve our technology! We welcome all contributions and hope that together we can build the future of connected Linux devices.
                        </p>
                      </div>
                      <div className="col-1 col-md-3" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row contact text-center" id="contact_us">
          <div className="container">
            <div className="row">
              <header className="col-12">
                <h2>Contact Us</h2>
              </header>
              <div className="col-12">
                <div className="row">
                  <div className="col-4 col-md-5" />
                  <div className="col-4 col-md-2">
                    <hr />
                  </div>
                  <div className="col-4 col-md-5" />
                </div>
              </div>
              <div className="col-12">
                <div className="row">
                  <div className="col-2 col-md-4" />
                  <p className="col-8 col-md-4">
                    Questions, feedback or want to explore how our technology can help?
                    We love to hear from you, so just shoot us a mail.<br/><br/>
                    <a href="mailto:team@pantahub.com">team@pantahub.com</a>
                  </p>
                  <div className="col-2 col-md-4" />
                </div>
              </div>
              {/* This is the contact form
              <div className="col-12">
                <div className="row">
                  <div className="col-1 col-md-3" />
                  <div className="col-10 col-sm-6 form">
                    <div className="input-group">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Leave your email"
                      />
                      <span className="input-group-btn">
                        <button className="btn btn-dark">Send</button>
                      </span>
                    </div>
                  </div>
                  <div className="col-1 col-md-3" />
                </div>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  state => state.auth,
  dispatch => ({
    onLogout: () => dispatch(logout())
  })
)(withAuthenticatedEvent(LandingPage, (isLoggedIn) => (isLoggedIn ? 'Logged User Home' : 'Anonimous Home')))
