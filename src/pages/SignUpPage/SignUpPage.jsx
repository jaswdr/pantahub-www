import React, { Component } from 'react'
import SignUpForm from '../../components/SignUpForm/SignUpForm'

class SignUpPage extends Component {
  render () {
    return (
      <div className="container">
        <br />
        <div className="row justify-content-center">
          <div className="col-md-6">
            <div className="form-registration-box">
              <div className="card">
                <div className="card-body">
                  <h1 key="title">Create Account</h1>
                  <h4 key="subtitle" className="text-muted">
                    Join Pantahub and start rocking your device!
                  </h4>
                  <p key="help_text">
                    Please complete the following information to create your
                    account. You will get a confirmation email with a link you
                    should follow to finish the process.
                  </p>
                  <SignUpForm />
                </div>
              </div>
            </div>
          </div>
        </div>
        <br />
        <br />
      </div>
    )
  }
}

export default SignUpPage
