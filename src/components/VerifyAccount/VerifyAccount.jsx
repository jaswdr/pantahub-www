/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'

import { connect } from 'react-redux'

import { Link } from 'react-router-dom'

import queryString from 'query-string'

import Loading from '../Loading/Loading'

import { loginPath } from '../../router/routes'

import { verify } from '../../store/auth/actions'

class VerifyAccount extends Component {
  componentDidMount () {
    const { verify, location } = this.props

    const { id, challenge } = queryString.parse(location.search)

    if (id && challenge) verify(id, challenge)
  }

  render () {
    const { loading, error, location } = this.props
    const { id, challenge } = queryString.parse(location.search)

    return (
      <div className="container">
        <br />
        <div className="row justify-content-center">
          <div className="col-md-6">
            <div className="form-registration-box">
              <div className="card">
                <div className="card-body">
                  {id && challenge
                    ? loading
                      ? [
                        <h1 key="title">Verifying account...</h1>,
                        <Loading key="loading" />
                      ]
                      : error !== null
                        ? [
                          <h1 key="title">Account not verified</h1>,
                          <p key="description">
                              Your account could not be verified. Please{' '}
                            <a href="mailto:">Contact Us</a> now to get your
                              account ready.
                          </p>
                        ]
                        : [
                          <h1 key="title">Account Verified</h1>,
                          <h4 key="subtitle" className="text-muted">
                              Your account is now ready
                          </h4>,
                          <p key="description">
                              You can now <Link to={loginPath}>Log In</Link> and
                              start using PantaHub!
                          </p>
                        ]
                    : [
                      <h1 key="title">Invalid Verification URL</h1>,
                      <p key="description">
                          The link you used to verify your account is not valid.
                          Please verify it is the same one you got on email or
                          please <a href="mailto:">Contact Us</a> now to get
                          your account ready.
                      </p>
                    ]}
                </div>
              </div>
            </div>
          </div>
        </div>
        <br />
        <br />
      </div>
    )
  }
}

export default connect(
  state => state.auth.verification,
  dispatch => ({
    verify: (id, challenge) => dispatch(verify(id, challenge))
  })
)(VerifyAccount)
