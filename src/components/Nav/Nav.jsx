/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'

import { connect } from 'react-redux'

import { NavLink } from 'react-router-dom'

import { logout } from '../../store/auth/actions'
import { navToggle } from '../../store/nav/actions'

import { loginPath, signUpPath, userDashboardPath, aboutUs } from '../../router/routes'

import pantacorAvatar from '../../assets/images/pantacor-avatar.png'

class Nav extends Component {
  render () {
    const { auth, nav, onLogout, onToggle } = this.props
    const { username, token } = auth
    const { show } = nav
    const loggedIn = token !== null

    return (
      <nav
        id="navbar-main"
        className="navbar navbar-expand-lg navbar-dark bg-primary"
      >
        <div className="container">
          <NavLink className="navbar-brand" to="/">
            <img alt="Pantahub" src={pantacorAvatar} width="30" />
          </NavLink>
          <button
            className="navbar-toggler"
            type="button"
            aria-expanded={show}
            aria-label="Toggle navigation"
            onClick={onToggle}
          >
            <span className="navbar-toggler-icon" />
          </button>

          <div
            className={`collapse navbar-collapse navigation-main ${show
              ? 'show'
              : ''}`}
          >
            <ul className="navbar-nav mr-auto navbar-main">
              <li className="nav-item">
                <NavLink
                  className="nav-link nav-link-account"
                  to={`${aboutUs}`}
                >
                  About
                </NavLink>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="https://docs.pantahub.com">Documentation</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="https://gitlab.com/pantacor">Sources</a>
              </li>
            </ul>
            {loggedIn && (
              <ul className="navbar-nav ml-auto navbar-profile">
                <li className="nav-item">
                  <NavLink
                    className="nav-link nav-link-account"
                    to={`${userDashboardPath}/${username}`}
                  >
                    Dashboard
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    to={`${userDashboardPath}/${username}/devices`}
                  >
                    Devices
                  </NavLink>
                </li>
                {/*
                <li className="nav-item">
                  <NavLink
                    className="nav-link nav-link-account"
                    to={`${userDashboardPath}/${username}/account`}
                  >
                    Account
                  </NavLink>
                </li> */}
                <li className="nav-item">
                  <button onClick={onLogout} className="btn btn-link nav-link">
                    Logout
                  </button>
                </li>
              </ul>
            )}
            {!loggedIn && (
              <ul className="navbar-nav ml-auto navbar-profile">
                <li className="nav-item">
                  <NavLink className="nav-link nav-link-account" to={loginPath}>
                    Log In
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink to={signUpPath} className="nav-link">
                    Sign Up
                  </NavLink>
                </li>
              </ul>
            )}
          </div>
        </div>
      </nav>
    )
  }
}

export default connect(
  state => ({ auth: state.auth, nav: state.nav }),
  dispatch => ({
    onLogout: () => dispatch(logout()),
    onToggle: () => dispatch(navToggle())
  })
)(Nav)
