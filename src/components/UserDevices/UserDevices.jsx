/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'

import orderBy from 'lodash.orderby'

import dayjs from 'dayjs'

import { connect } from 'react-redux'

import { Link } from 'react-router-dom'

import {
  initializeDevices,
  setDevicesSearch,
  setDevicesSortField
} from '../../store/devices/actions'

import { CopyButton } from '../ClipboardFields/ClipboardFields'
import { userDashboardPath } from '../../router/routes'
import { classForDevStatus } from '../../lib/utils'
import { DeviceStatus, DeviceStatusMessage } from '../UserDevice/DeviceStatus'
import Loading from '../Loading/Loading'
import TruncatedText from '../TruncatedText/TruncatedText'
import DeviceDeleteButton from '../DeviceDeleteButton/DeviceDeleteButton'
import DevicePublishButton from '../DevicePublishButton/DevicePublishButton'
import RelativeTime from '../RelativeTime/RelativeTime'
import { useInterval } from '../../hooks/useInterval'
import { resolvePath } from '../../lib/object.helpers'

class UserDevicesHeader extends Component {
  render () {
    const { onSearch, search } = this.props

    return (
      <div className="header-sector">
        <div className="row align-items-center">
          <div className="col-md-3">
            <h1>Devices</h1>
          </div>
          <div className="col-md-9">
            <div className="input-group col-m">
              <input
                type="text"
                className="form-control"
                placeholder="Search in Your Devices"
                aria-label="Search in Your Devices"
                onChange={evt => {
                  onSearch(evt.target.value)
                }}
                value={search || ''}
              />
              <span className="input-group-append">
                <button className="btn btn-light" type="button">
                  <i className="mdi mdi-magnify" aria-hidden="true" />
                </button>
              </span>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

class UserDevicesTable extends Component {
  render () {
    const {
      username,
      token,
      devices,
      loading,
      error,
      search,
      toggleSort,
      sortField,
      sortDirection,
      deleteTracker,
      publishTracker
    } = this.props

    const _sortFuncs = {
      'step-time': v => dayjs() - dayjs(v['step-time'])
    }

    const deviceRows = devices.length === 0 && loading ? (
      <tr>
        <td colSpan={6}>
          <Loading />
        </td>
      </tr>
    ) : error !== null ? (
      <tr>
        <td colSpan={6}>
          <span className="error">
            An error occurred while loading the devices information. Please try
            again later.
          </span>
        </td>
      </tr>
    ) : (
      orderBy(
        devices.filter(
          dev => ((search && dev['device-nick']) || '').indexOf(search) >= 0
        ),
        [_sortFuncs[sortField] || sortField],
        [sortDirection ? 'asc' : 'desc']
      ).map(dev => (
        <tr key={dev.deviceid} className="device-nick">
          <td>
            <div className={`status-led bg-${classForDevStatus(dev.status)}`} />{' '}
            <Link
              to={`${userDashboardPath}/${username}/devices/${dev.deviceid}`}
            >
              <TruncatedText text={dev['device-nick']} />
              {dev.public && (
                <i
                  title="This device is public"
                  className="mdi mdi-earth"
                  aria-hidden="true"
                />
              )}{' '}
            </Link>
          </td>
          <td className="progress-revision text-monospace">
            {dev['state-sha'] ? (
              <React.Fragment>
                <span className="progress-revision-commit">
                  {dev['state-sha'].slice(0, 8)}{' '}
                </span>
                <span className="progress-revision-rev text-muted">
                      ({dev['progress-revision']})
                </span>
              </React.Fragment>
            ) : (
              <span className="progress-revision-rev">
                {dev['progress-revision']}
              </span>
            )}
          </td>
          <td className="step-time">
            <RelativeTime when={dev['step-time']} />
          </td>
          <td className="timestamp">
            <RelativeTime when={dev['timestamp']} />
          </td>
          <td className="status">
            <DeviceStatus status={dev.status} statusMsg={dev['status-msg']} />
          </td>
          <td className="status-msg">
            <DeviceStatusMessage status={dev.status} statusMsg={dev['status-msg']} />
          </td>
          <td className="text-right actions">
            <div className="btn-group" role="group" aria-label="Device actions">
              <DevicePublishButton
                token={token}
                deviceId={dev.deviceid}
                isPublic={dev.public}
                loading={(publishTracker[dev.deviceid] || {}).loading}
                compact
              />
              <CopyButton
                className="btn btn-sm btn-light"
                title="Copy Share URL"
                value={`https://www.pantahub.com/${username}/${
                  dev.deviceid
                }/step/${dev['progress-revision']}`}
              >
                <i className="mdi mdi-link" aria-hidden="true" />
              </CopyButton>
              <DeviceDeleteButton
                username={username}
                token={token}
                deviceId={dev.deviceid}
                compact
                loading={(deleteTracker[dev.deviceid] || {}).loading}
                redirectTo={null}
              />
            </div>
          </td>
        </tr>
      ))
    )

    const sortIconForField = field =>
      sortField === field
        ? sortDirection
          ? 'chevron-down'
          : 'chevron-up'
        : 'unfold-more-horizontal'

    const sortFieldActive = field => (sortField === field ? 'active' : null)

    const sortableFields = [
      ['device-nick', 'Device'],
      ['progress-revision', 'Commit ID (Rev)'],
      ['step-time', 'Modified'],
      ['timestamp', 'Last seen'],
      ['status', 'Status'],
      ['status-msg', 'Message']
    ]

    return (
      <div className="list-sector">
        <table className="table table-hover">
          <thead>
            <tr>
              {sortableFields.map(([field, label]) => (
                <th
                  key={field}
                  onClick={toggleSort(field)}
                  className={`${sortFieldActive(field)} ${field}`}
                >
                  <span>
                    {label}{' '}
                    <i
                      className={`mdi mdi-${sortIconForField(field)}`}
                      aria-hidden="true"
                    />
                  </span>
                </th>
              ))}
              <th className="text-right actions">Actions</th>
            </tr>
          </thead>
          <tbody>{deviceRows}</tbody>
        </table>
      </div>
    )
  }
}

class UserDevicesInner extends Component {
  render () {
    const {
      username,
      token,
      devices,
      loading,
      deleteTracker,
      publishTracker,
      onSearch,
      search,
      toggleSort,
      sortField,
      sortDirection,
      error
    } = this.props

    return (
      <React.Fragment>
        <UserDevicesHeader
          onSearch={onSearch}
          loading={loading}
          search={search}
        />
        <UserDevicesTable
          username={username}
          token={token}
          devices={devices}
          loading={loading}
          search={search}
          toggleSort={toggleSort}
          sortField={sortField}
          sortDirection={sortDirection}
          deleteTracker={deleteTracker}
          publishTracker={publishTracker}
          error={error}
        />
      </React.Fragment>
    )
  }
}

function UserDevices (props) {
  useInterval(() => {
    const { init, auth } = props
    const { token } = auth
    if (!props.devs.search) {
      init(token)
    }
  })

  const { auth, devs, dash, onSearch, toggleSort } = props
  const { username, token } = auth
  const {
    devices,
    loading,
    search,
    sortField,
    sortDirection,
    error: devsError
  } = devs
  const quotaStats = resolvePath(dash, 'data.subscription.quota-stats', {})
  const devicesQuota = quotaStats['DEVICES'] || null

  const actualDeviceCount = (devicesQuota || {})['Actual'] || 0
  const maxDeviceCount = (devicesQuota || {})['Max'] || 0

  return (
    <React.Fragment>
      <div
        key="breadcrumbs"
        className="breadcrumb-sector row align-items-center"
      >
        <div className="col-6">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to={`${userDashboardPath}/${username}`}>Dashboard</Link>
            </li>
            <li className="breadcrumb-item active">Devices</li>
          </ol>
        </div>
        <div className="col-6 mr-auto text-right">
          {devicesQuota !== null && (
            <span className="quota-indicator">
              You are using {actualDeviceCount} of {maxDeviceCount} devices
              available
            </span>
          )}
          <Link
            to={`${userDashboardPath}/${username}/claim`}
            className="btn btn btn-dark"
          >
            Claim a Device
          </Link>
        </div>
      </div>

      <div key="devicesInner" className="row">
        <div className="col-md-12">
          <UserDevicesInner
            username={username}
            token={token}
            devices={devices}
            loading={loading}
            onSearch={onSearch}
            search={search}
            toggleSort={toggleSort}
            sortField={sortField}
            sortDirection={sortDirection}
            deleteTracker={devs.delete}
            publishTracker={devs.publish}
            error={devsError}
          />
        </div>
      </div>
    </React.Fragment>
  )
}

export default connect(
  state => state,
  dispatch => ({
    init: token => dispatch(initializeDevices(token)),
    onSearch: search => dispatch(setDevicesSearch(search)),
    toggleSort: field => () => dispatch(setDevicesSortField(field))
  })
)(UserDevices)
