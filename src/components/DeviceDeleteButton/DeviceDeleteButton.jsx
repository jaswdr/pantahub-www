/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'

import { connect } from 'react-redux'

import { withRouter } from 'react-router-dom'

import { userDashboardPath } from '../../router/routes'

import { deviceDelete } from '../../store/devices/actions'

class DeviceDeleteButton extends Component {
  state = {
    confirmation: false
  };

  _onConfirm () {
    this.setState({
      confirmation: true
    })
  }

  _onCancel () {
    this.setState({
      confirmation: false
    })
  }

  async _onDelete () {
    const {
      history,
      dispatch,
      username,
      token,
      deviceId,
      redirectTo = `${userDashboardPath}/${username}/devices`
    } = this.props
    await this.setState({
      confirmation: false
    })
    await dispatch(deviceDelete(token, deviceId))
    if (redirectTo) history.push(redirectTo)
  }

  render () {
    const { compact, loading, disabled } = this.props
    const { confirmation } = this.state

    // const disabled = true;

    let buttons = [
      <button
        key="button"
        className={`btn btn-sm btn-danger ${disabled && 'btn-disabled'}`}
        onClick={
          confirmation ? this._onDelete.bind(this) : this._onConfirm.bind(this)
        }
        disabled={loading || disabled}
      >
        <i
          className={`mdi mdi-${
            loading ? 'refresh pantahub-loading' : 'delete-forever'
          }`}
          aria-hidden="true"
        />{' '}
        {confirmation ? 'Confirm?' : compact ? '' : 'Delete'}
      </button>
    ]
    if (confirmation) {
      buttons.push(
        <button
          key="confirm"
          className="btn btn-sm btn-info"
          onClick={this._onCancel.bind(this)}
          disabled={loading}
        >
          Cancel
        </button>
      )
    }
    return buttons
  }
}

export default connect(state => state.devs.delete)(
  withRouter(DeviceDeleteButton)
)
