/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { TextInputWithClipboard } from '../ClipboardFields/ClipboardFields'
import DeviceDeleteButton from '../DeviceDeleteButton/DeviceDeleteButton'
import DevicePublishButton from '../DevicePublishButton/DevicePublishButton'
import { DeviceStatus, DeviceStatusMessage } from './DeviceStatus'
import RelativeTime from '../RelativeTime/RelativeTime'
import { resolvePath } from '../../lib/object.helpers'
import IsAuthorized from '../IsAuthorized/IsAuthorized'

const WWW_URL = process.env.REACT_APP_WWW_URL || 'https://www.pantahub.com'
const PVR_URL = process.env.REACT_APP_PVR_URL || 'https://pvr.pantahub.com'

function UserDeviceInfo (props) {
  const { deviceId, device, username, token, disabled } = props
  const { history = {}, summary = {} } = device
  const currentStep = history.currentStep
  const status = currentStep
    ? resolvePath(currentStep, 'progress.status', '')
    : resolvePath(summary, 'status', '')
  const statusMsg = currentStep
    ? resolvePath(currentStep, 'progress.status-msg', '')
    : resolvePath(summary, 'status-msg', '')
  const sha = currentStep
    ? resolvePath(currentStep, 'state-sha', '').slice(0, 8)
    : resolvePath(summary, 'state-sha', '').slice(0, 8)
  const revision = currentStep ? currentStep.rev : null
  const lastModified = device['time-modified']
  const cloneURL = `${PVR_URL}/${device['owner-nick']}/${device.nick}/${revision}`
  const shareURL = `${WWW_URL}/u/${device['owner-nick']}/devices/${deviceId}/step/${revision}`

  return (
    <div className="device-detail-main">
      <div className="jumbotron">
        <div className="row">
          <div className="col-md-6">
            <table className="table table-sm borderless">
              <tbody>
                <tr>
                  <th>Device ID</th>
                  <td className="text-monospace">{deviceId}</td>
                </tr>
                {revision !== null && [
                  <tr key="revision">
                    <th>Commit ID (Rev)</th>
                    <td className="text-monospace">
                      {sha ? (
                        [
                          <span className="progress-revision-commit" key="progress-revision-commit">
                            {sha}
                          </span>,
                          <span className="progress-revision-rev text-muted" key="progress-revision-rev">
                            ({revision})
                          </span>
                        ]
                      ) : (
                        <span className="progress-revision-rev">
                          {revision}
                        </span>
                      )}
                    </td>
                  </tr>,
                  <tr key="cloneURL">
                    <th>Clone URL</th>
                    <td>
                      <TextInputWithClipboard
                        value={cloneURL}
                        label="Clone URL"
                        disabled={disabled}
                      />
                    </td>
                  </tr>,
                  <tr key="shareURL">
                    <th>Share URL</th>
                    <td>
                      <TextInputWithClipboard
                        value={shareURL}
                        label="Share URL"
                        disabled={disabled}
                      />
                    </td>
                  </tr>
                ]}
              </tbody>
            </table>
          </div>
          <div className="col-md-6">
            <table className="table table-sm borderless">
              <tbody>
                <IsAuthorized owner={device.owner}>
                  <tr>
                    <th>Actions</th>
                    <td>
                      <div className="btn-group">
                        <DevicePublishButton
                          username={username}
                          token={token}
                          deviceId={deviceId}
                          isPublic={device.public || false}
                          disabled={disabled}
                        />
                        <DeviceDeleteButton
                          username={username}
                          token={token}
                          deviceId={deviceId}
                          label={'Delete'}
                          disabled={disabled}
                        />
                      </div>
                    </td>
                  </tr>
                </IsAuthorized>
                {status !== null && (
                  <tr>
                    <th>Status</th>
                    <td>
                      <DeviceStatus status={status} />
                    </td>
                  </tr>
                )}
                {statusMsg !== null && (
                  <tr>
                    <th>Message</th>
                    <td>
                      <DeviceStatusMessage status={status} statusMsg={statusMsg} />
                    </td>
                  </tr>
                )}
                <tr>
                  <th>Last Modified</th>
                  <td>
                    <RelativeTime when={lastModified} />
                  </td>
                </tr>
                <tr>
                  <th />
                  <td />
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}

export default UserDeviceInfo
