/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { resolvePath } from '../../lib/object.helpers'
import { userDashboardPath } from '../../router/routes'
import Loading from '../Loading/Loading'
import UserDeviceHeader from './Header'
import UserDeviceInfo from './Info'
import UserDeviceNavigator from '../Navigator'
import {
  initializeDevice,
  devicePublish,
  deviceNavigatorSetTab,
  deviceSetLogsFilter,
  deviceSetEditing
} from '../../store/devices/actions'

import './user_device.scss'

class UserDeviceInner extends Component {
  render () {
    const {
      device,
      deviceId,
      dispatch,
      onChangeNavigatorTab,
      navigator,
      onSetEditing,
      onSetLogsFilter,
      activeTab,
      username,
      token,
      disabled,
      saving
    } = this.props

    return (
      <React.Fragment>
        <UserDeviceHeader
          username={username}
          device={device}
          deviceId={deviceId}
          disabled={disabled}
          editigHandler={onSetEditing}
          saving={saving}
          dispatch={dispatch}
          token={token}
        />
        <UserDeviceInfo
          device={device}
          deviceId={deviceId}
          username={username}
          token={token}
          disabled={disabled}
        />
        <UserDeviceNavigator
          onChangeNavigatorTab={onChangeNavigatorTab}
          navigator={navigator}
          onSetLogsFilter={onSetLogsFilter}
          activeTab={activeTab}
          device={device}
          username={username}
          token={token}
          dispatch={dispatch}
          disabled={disabled}
        />
      </React.Fragment>
    )
  }
}

class UserDevice extends Component {
  constructor (props) {
    super(props)
    this.timerId = null
    this.startPulling = this.startPulling.bind(this)
  }

  componentDidMount () {
    window.requestAnimationFrame(() => {
      this.getData().then(() => this.startPulling())
    })
  }

  componentWillUnmount () {
    this.stopPulling()
  }

  shouldComponentUpdate (nextProps) {
    if (nextProps.loading) {
      return true
    }
    const rev = resolvePath(nextProps, 'current.device.history.currentStep.rev')
    const stateDevice = resolvePath(nextProps, 'current.device.id')
    const nextRevision = parseInt(nextProps.match.params.revision, 10)
    const currentRevision = parseInt(this.props.match.params.revision, 10)
    const sameRevision = !isNaN(nextRevision) && !isNaN(currentRevision) && nextRevision === currentRevision
    const currentDevice = this.props.match.params.deviceId
    const nextDevice = nextProps.match.params.deviceId
    const differentDevice = currentDevice === nextDevice && stateDevice !== nextDevice
    const differentRevision = sameRevision && !isNaN(currentRevision) && rev && rev !== currentRevision
    if (differentRevision || differentDevice) {
      return false
    }
    return true
  }

  componentDidUpdate (prevProps) {
    const nextRevision = parseInt(this.props.match.params.revision, 10)
    const currentRevision = parseInt(prevProps.match.params.revision, 10)
    const nextDevice = this.props.match.params.deviceId
    const currentDevice = prevProps.match.params.deviceId
    const differentDevice = currentDevice && nextDevice && currentDevice !== nextDevice
    const differentRevision = !isNaN(nextRevision) && !isNaN(currentRevision) && nextRevision !== currentRevision
    if (differentRevision || differentDevice) {
      this.stopPulling()
      this.getData().then(() => this.startPulling())
    }
  }

  getData (refresher = false) {
    const { init, auth, match, current } = this.props
    const { timestamp, device } = current
    const { deviceId, revision } = match.params
    const { token } = auth
    return init(token, timestamp, deviceId, revision, device, refresher)
  }

  startPulling () {
    this.timerId = setInterval(() => {
      if (!this.props.initializing && !this.props.current.editing) {
        this.getData(true)
      }
    }, process.env.REACT_APP_REFRESH_RATE || 3000)
  }

  stopPulling () {
    if (this.timerId !== null) {
      window.clearInterval(this.timerId)
    }
    this.timerId = null
    window.__PH_CDLLETS = undefined
  }

  render () {
    const {
      onChangeNavigatorTab,
      onSetEditing,
      onSetLogsFilter,
      match,
      auth,
      dispatch,
      current,
      loading,
      saving: devsSaving
    } = this.props
    const { deviceId } = match.params
    const { device, navigator } = current
    const { username, token } = auth
    const { activeTab } = navigator

    const saving = (devsSaving || {})[deviceId]

    return [
      <div
        key="breadcrumbs"
        className="breadcrumb-sector row align-items-center"
      >
        <div className="col-12">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to={`${userDashboardPath}/${username}`}>Dashboard</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to={`${userDashboardPath}/${username}/devices`}>
                Devices
              </Link>
            </li>
            <li className="breadcrumb-item active">
              {device ? device.nick : deviceId}
            </li>
          </ol>
        </div>
      </div>,

      <div key="deviceBody" className="row">
        <div className="col-md-12">
          {loading ? (
            <Loading key="spinner" />
          ) : (
            <UserDeviceInner
              device={device || {}}
              deviceId={deviceId}
              disabled={loading}
              dispatch={dispatch}
              username={username}
              token={token}
              navigator={navigator}
              onChangeNavigatorTab={onChangeNavigatorTab}
              onSetEditing={onSetEditing}
              onSetLogsFilter={onSetLogsFilter}
              activeTab={activeTab}
              saving={saving}
            />
          )}
        </div>
      </div>
    ]
  }
}

export default connect(
  state => ({
    current: state.devs.current,
    loading: state.devs.loading,
    refreshing: state.devs.refreshing,
    saving: state.devs.devsSaving,
    auth: state.auth,
    initializing: state.devs.initializing
  }),
  (dispatch, props) => ({
    init: (...params) => dispatch(initializeDevice(...params)),
    onPublish: (token, deviceId) => dispatch(devicePublish(deviceId)),
    onChangeNavigatorTab: tab => () => dispatch(deviceNavigatorSetTab(tab)),
    onSetLogsFilter: logsFilter => dispatch(deviceSetLogsFilter(logsFilter)),
    onSetEditing: editing => dispatch(deviceSetEditing(editing)),
    dispatch // FIXME: implement more adequate handlers
  })
)(UserDevice)
