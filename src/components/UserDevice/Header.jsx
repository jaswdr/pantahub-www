/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { PureComponent } from 'react'

import { Link, withRouter } from 'react-router-dom'

import { devicePatch } from '../../store/devices/actions'
import { userDashboardPath } from '../../router/routes'
import { classForDevStatus } from '../../lib/utils'
import EditableElement from '../Editable/Editable'

class UserDeviceHeader extends PureComponent {
  handleStepNavigation = (event) => {
    const { username, deviceId, history } = this.props
    history.push(`${userDashboardPath}/${username}/devices/${deviceId}/step/${event.target.value}`)
  }

  renderStepSelector () {
    const { device } = this.props
    const { history = {} } = device
    const { steps = [], revision } = history

    if (steps.length > 1) {
      return (
        <li className="page-item active">
          <select className="form-control" defaultValue={revision} onChange={this.handleStepNavigation}>
            {steps.map((_, index) => (<option key={index} value={index}>{index}</option>))}
          </select>
        </li>
      )
    }

    return (
      <li className="page-item active">
        <span className="page-link">
          {revision}
          <span className="sr-only">(current)</span>
        </span>
      </li>
    )
  }

  onEditSave = (nick) => {
    const {
      dispatch,
      token,
      deviceId
    } = this.props
    dispatch(devicePatch(token, deviceId, { nick }))
  }

  render () {
    const {
      username,
      device = {},
      deviceId,
      disabled,
      editigHandler,
      saving
    } = this.props
    const { nick, history = {} } = device
    const { currentStep, steps = [], revision, revIndex, lastRevision } = history
    const prevIndex = revIndex > 0 ? revIndex - 1 : revIndex
    const nextIndex = revIndex < steps.length ? revIndex + 1 : revIndex
    const prevStep = steps[prevIndex] || {}
    const nextStep = steps[nextIndex] || {}

    const status = currentStep
      ? currentStep.progress
        ? currentStep.progress.status
        : null
      : null
    const isLast = revision === lastRevision
    const isFirst = revision === 0

    return (
      <div className="header-sector">
        <div className="row">
          <div className="col-md-8">
            <h1 className="device-nick">
              <EditableElement
                el="span"
                name="device-nick"
                value={nick || deviceId}
                editigHandler={editigHandler}
                saving={saving}
                truncate={true}
                saveHandler={this.onEditSave}
              />{' '}
              {device.public && (
                <i className="mdi mdi-earth" aria-hidden="true" />
              )}
              {status && (
                <span
                  className={`status-badge status-${status.toLowerCase()} badge badge-${classForDevStatus(
                    status
                  )}`}
                >
                  {status}
                </span>
              )}
            </h1>
          </div>
          <div className="col-md-4 text-right">
            <div className="device-revision-pager">
              <strong>Revision:</strong>
              <nav
                className="d-inline-block"
                aria-label="Revisions navigator"
              >
                <ul className="pagination">
                  <li className="page-item">
                    <Link
                      className="page-link"
                      to={`${userDashboardPath}/${username}/devices/${deviceId}/step/0`}
                    >
                      <span aria-hidden="true">
                        <i className="mdi mdi-chevron-double-left"></i>
                      </span>
                      <span className="sr-only"> First</span>
                    </Link>
                  </li>
                  <li className="page-item">
                    <Link
                      disabled={isFirst || disabled}
                      className="page-link"
                      to={`${userDashboardPath}/${username}/devices/${deviceId}/step/${
                        prevStep.rev || 0
                      }`}
                    >
                      <span aria-hidden="true">
                        <i className="mdi mdi-chevron-left"></i>
                      </span>
                      <span className="sr-only"> Previous</span>
                    </Link>
                  </li>
                  {this.renderStepSelector()}
                  <li className="page-item">
                    <Link
                      disabled={isLast || disabled}
                      className="page-link"
                      to={`${userDashboardPath}/${username}/devices/${deviceId}/step/${
                        nextStep.rev || lastRevision
                      }`}
                    >
                      <span aria-hidden="true">
                        <i className="mdi mdi-chevron-right"></i>
                      </span>
                      <span className="sr-only">Next </span>
                    </Link>
                  </li>
                  <li className="page-item">
                    <Link
                      className="page-link"
                      to={`${userDashboardPath}/${username}/devices/${deviceId}/step/${steps.length - 1}`}
                    >
                      <span aria-hidden="true">
                        <i className="mdi mdi-chevron-double-right"></i>
                      </span>
                      <span className="sr-only">Last</span>
                    </Link>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(UserDeviceHeader)
