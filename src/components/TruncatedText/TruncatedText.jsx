import React, { useState, useCallback } from 'react'
import './truncated-text.scss'

export default function TruncatedText ({ text = '', size = 20 }) {
  const [show, setShow] = useState(false)
  const needsTruncated = text.length > size
  const trucated = needsTruncated ? text.substring(0, size) + ' ...' : text + ' '
  const fullText = show && needsTruncated
    ? (<span className="trucated-text__full-text">{text}</span>)
    : null
  const open = useCallback(() => setShow(true))
  const close = useCallback(() => setShow(false))
  return (
    <span className="trucated-text" onMouseEnter={open} onMouseLeave={close}>
      {trucated}
      {fullText}
    </span>
  )
}
