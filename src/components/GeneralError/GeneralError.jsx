import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { clearError } from '../../store/general-errors/actions'
import { HTTP_STATUS } from '../../lib/api.helpers'
import './general_error.scss'

function ErrorMessage ({ type = 'error', message, dismissError }) {
  const errorClass = `general-errors general-errors--${type}`

  useEffect(() => {
    setTimeout(() => { dismissError() }, 2000)
  }, [])
  return (
    <div className={errorClass}>
      <div className="general-errors__wrapper">
        <span className="general-errors__message">
          {message}
        </span>
        <button className="general-errors__close btn btn-link" onClick={dismissError}>
          <i className="mdi mdi-close-box"></i>
        </button>
      </div>
    </div>
  )
}

function GeneralError ({ errors, dismissError }) {
  if (errors.type === HTTP_STATUS.FORBIDDEN) {
    return (
      <ErrorMessage
        message="Your don't have permissions to this resource"
        dismissError={dismissError}
      />
    )
  }
  return null
}

export default connect(
  state => ({ errors: state.errors }),
  dispatch => ({
    dismissError: (event) => {
      if (event && event.preventDefault) {
        event.preventDefault()
      }
      dispatch(clearError())
    }
  })
)(GeneralError)
