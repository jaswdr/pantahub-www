/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'

import { initializeDashboard } from '../../store/dashboard/actions'
import { logout } from '../../store/auth/actions'
import UserDashboardInner from './UserDashboardInner'
import { resolvePath } from '../../lib/object.helpers'
import HowToStart from '../HowToStart/HowToStart'
import { withAuthenticatedEvent } from '../../store/ga-middleware'

class UserDashboard extends Component {
  componentDidMount () {
    const { init, logout, auth } = this.props
    try {
      init(auth.token)
    } catch (err) {
      logout()
    }
  }

  render () {
    const { auth, dash } = this.props
    const { username } = auth
    const { data, loading } = dash
    const topDevices = data['top-devices'] || []
    const subscription = data['subscription'] || {}
    const deviceCount = resolvePath(subscription, 'quota-stats.DEVICES.Actual', 0)

    if (deviceCount === 0) {
      return (<HowToStart {...this.props} />)
    }

    return (
      <React.Fragment>
        <div
          key="breadcrumbs"
          className="breadcrumb-sector row align-items-center"
        >
          <div className="col-sm-12">
            <ol className="breadcrumb">
              <li className="breadcrumb-item active">Dashboard</li>
            </ol>
          </div>
        </div>
        <UserDashboardInner
          key="dashboardInner"
          username={username}
          loading={loading}
          topDevices={topDevices}
          subscription={subscription}
        />
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({ auth: state.auth, dash: state.dash })
const mapDispatchToProps = dispatch => ({
  init: token => dispatch(initializeDashboard(token)),
  logout: () => dispatch(logout())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withAuthenticatedEvent(UserDashboard, (isLoggedIn) => (isLoggedIn ? 'Logged User Dashboard' : 'Anonimous Dashboard')))
