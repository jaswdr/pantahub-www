/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import Loading from '../Loading/Loading'
import { Link } from 'react-router-dom'
import TruncatedText from '../TruncatedText/TruncatedText'
import { DeviceStatus } from '../UserDevice/DeviceStatus'
import { userDashboardPath } from '../../router/routes'

export default class UserDashboardTopDevices extends Component {
  render () {
    const { username, topDevices, loading } = this.props

    const devicesTable = loading ? (
      <Loading key="loading" />
    ) : (
      <table className="table" key="topDevices">
        <thead className="thead-default">
          <tr>
            <th className="col-md-8">Nick</th>
            <th className="col-md-4">Status</th>
          </tr>
        </thead>
        <tbody>
          {topDevices.map(device => (
            <tr key={device.nick}>
              <td>
                <Link
                  to={`${userDashboardPath}/${username}/devices/${device['device-id'] || ''}`}
                >
                  <TruncatedText text={device.nick} size={30}/>
                </Link>{' '}
                {device.public && (
                  <i className="mdi mdi-earth" aria-hidden="true" />
                )}
              </td>
              <td className="text-left">
                <DeviceStatus status={device.status || device.type} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    )

    return (
      <React.Fragment>
        {devicesTable}
        <Link
          className="btn btn-dark btn-block"
          to={`${userDashboardPath}/${username}/devices`}
          key="devicesLink"
        >
          View All Your Devices
        </Link>
      </React.Fragment>
    )
  }
}
