/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import UserDashboardSubscriptionCharts from './UserDashboardSubscriptionCharts'
import UserDashboardTopDevices from './UserDashboardTopDevices'
import { resolvePath } from '../../lib/object.helpers'

export default class UserDashboardData extends Component {
  render () {
    const { username, loading, topDevices, subscription, className } = this.props
    const deviceCount = resolvePath(subscription, 'quota-stats.DEVICES.Actual', 0)

    return (
      <div className={`dashboard-sector bg-light ${className || ''}`}>
        <h3>Dashboard</h3>
        <hr />
        <div className="quota-box">
          <div className="row">
            <div className="col-md-12 text-center">
              <h4>Your Quota</h4>
            </div>
          </div>
          <UserDashboardSubscriptionCharts
            subscription={subscription}
            loading={loading}
          />
          {deviceCount > 0 && (
            <React.Fragment>
              <div className="row">
                <div className="col-md-12 text-center">
                  <h4>Your Top Devices</h4>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 table-top-devices">
                  <UserDashboardTopDevices
                    username={username}
                    topDevices={topDevices}
                    loading={loading}
                  />
                </div>
              </div>
            </React.Fragment>
          )}
        </div>
      </div>
    )
  }
}
