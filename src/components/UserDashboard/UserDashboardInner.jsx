/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import UserDashboardInfo from './UserDashBoardInfo'
import UserDashboardNews from './UserDashboardNews'
import UserDashboardData from './UserDashboardData'

export default class UserDashboardInner extends Component {
  render () {
    const { username, loading, topDevices, subscription } = this.props

    return (
      <div key="dashboard" className="row">
        <div className="col-md-6 col-lg-7">
          <div className="row">
            <div className="col-12">
              <UserDashboardInfo username={username} />
            </div>
            <div className="col-12 d-none d-md-block">
              <UserDashboardNews />
            </div>
          </div>
        </div>
        <div className="col-md-6 col-lg-5">
          <UserDashboardData
            username={username}
            loading={loading}
            topDevices={topDevices}
            subscription={subscription}
          />
        </div>
        <div className="col-12 d-md-none d-block">
          <UserDashboardNews />
        </div>
      </div>
    )
  }
}
