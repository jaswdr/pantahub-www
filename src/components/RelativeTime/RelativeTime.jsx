import React from 'react'
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'

dayjs.extend(relativeTime)

export default function RelativeTime ({ when }) {
  return (
    <span title={dayjs(when).format()}>
      {dayjs(when).fromNow()}
    </span>
  )
}
