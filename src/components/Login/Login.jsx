/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { useCallback } from 'react'

import { connect } from 'react-redux'

import { Link } from 'react-router-dom'

import { setUsername, setPassword, getToken } from '../../store/auth/actions'

import { signUpPath, requestNewPassword } from '../../router/routes'
import useFormValidation from '../../hooks/useFormValidation'

function Login (props) {
  const {
    dispatch,
    username,
    password,
    loading,
    getTokenError
  } = props
  const {
    formState,
    onSubmit,
    onInput,
    getError
  } = useFormValidation()

  const submit = useCallback((event) => {
    const { isValid } = onSubmit(event)
    if (isValid) {
      dispatch(getToken(username, password))
    }
  }, [username, password])

  const wasValidatedClass = formState.wasValidated ? 'was-validated' : null

  return (
    <div className="container">
      <br />
      <div className="row justify-content-center">
        <div className="col-md-6 ">
          <div className="form-registration-box">
            <div className="card">
              <div className="card-body">
                <h1>Log In</h1>
                {getTokenError !== null && (
                  <p key="errors" className="alert alert-danger">
                    {getTokenError['Error'] ||
                      (typeof getTokenError['message'] === 'string' &&
                        getTokenError['message']) ||
                      JSON.stringify(getTokenError)}
                  </p>
                )}
                <form
                  onSubmit={submit}
                  noValidate={true}
                  className={wasValidatedClass}
                >
                  <div className="form-group">
                    <label htmlFor="username">Username</label>
                    <input
                      type="text"
                      autoCapitalize="none"
                      className="form-control"
                      id="username"
                      name="username"
                      required="required"
                      onInput={onInput}
                      onChange={evt =>
                        dispatch(setUsername(evt.target.value))}
                      placeholder="Username"
                    />
                    <div className="invalid-feedback"> {getError('username')} </div>
                  </div>
                  <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input
                      type="password"
                      className="form-control"
                      id="password"
                      name="password"
                      required="required"
                      onInput={onInput}
                      onChange={evt =>
                        dispatch(setPassword(evt.target.value))}
                      placeholder="Password"
                    />
                    <div className="invalid-feedback"> {getError('password')} </div>
                  </div>
                  <button
                    type="submit"
                    className="btn btn-dark btn-lg btn-block"
                    disabled={loading}
                  >
                    {loading ? (
                      <span className="mdi mdi-refresh pantahub-loading" />
                    ) : (
                      ''
                    )}
                    Login
                  </button>
                </form>
                <br />
                <p>
                  <Link to={requestNewPassword}>Forgot your password?</Link>
                </p>
                <p className="text-right">
                  {'Don\'t have an account? '}
                  <Link to={signUpPath}>Sign Up now!</Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br />
      <br />
    </div>
  )
}

export default connect(state => state.auth)(Login)
