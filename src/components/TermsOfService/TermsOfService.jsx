/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React from 'react'

const content = [
  `The information contained on http://www.pantahub.com website (the "Service")
  is for general information and demo purposes only.`,
  `Pantacor Ltd. assumes no responsibility for errors or omissions in the
  contents on the Service. It further assumes no responsibility that any
  functionality available on the Service is suitable or fit for any purpose.`,
  `In no event shall Pantacor Ltd. be liable for any special, direct, indirect, consequential, 
  or incidental damages or any damages whatsoever, whether in an action of contract, 
  negligence or other tort, arising out of or in connection with the use of the Service 
  or the contents of the Service. Pantacor Ltd. reserves the right to make additions,
  deletions, or modification to the contents on the Service and may also terminate the 
  operation of the Service at any time without prior notice.`,
  `Pantacor Ltd. does not warrant that the website is free of viruses or other harmful
  components and does not take responsibility of the content shared by third parties through the Service.`
]

const TermsOfService = () => (
  <div className="container">
    <div className="row terms-of-service">
      <div className="col-12">
        <header>
          <h1>Disclaimer</h1>
        </header>
        <hr />
        <article>
          {content.map((value, key) => (<p key={key}>{value}</p>))}
        </article>
      </div>
    </div>
  </div>
)

export default TermsOfService
