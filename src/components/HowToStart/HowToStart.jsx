/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import UserDashboardNews from '../UserDashboard/UserDashboardNews'
import UserDashboardData from '../UserDashboard/UserDashboardData'
import HowToStartInner from './HowToStartInner'
import { resolvePath } from '../../lib/object.helpers'

export default function HowToStart (props) {
  const { auth, dash } = props
  const { username } = auth
  const { loading } = dash
  const topDevices = resolvePath(dash, 'data.top-devices', [])
  const subscription = resolvePath(dash, 'data.subscription', {})

  return (
    <div key="dashboard" className="row">
      <div className="col-12">
        <HowToStartInner username={username} />
      </div>
      <div className="row">
        <div className="col-md-6">
          <UserDashboardNews />
        </div>
        <div className="col-md-6">
          <UserDashboardData
            username={username}
            loading={loading}
            topDevices={topDevices}
            subscription={subscription}
          />
        </div>
      </div>
    </div>
  )
}
