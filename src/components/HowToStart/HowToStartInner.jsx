/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { Link } from 'react-router-dom'
import { userDashboardPath } from '../../router/routes'

import './how_to_start.scss'

export default function HowToStartInner (props) {
  const { username, className } = props
  return (
    <div className={`hts getting-started row ${className || ''}`}>
      <div className="col-12 pb-5">
        <h1 className="text-center pt-4">
          Welcome to Pantahub!
          <br />
          <small>Let&apos;s start connecting devices</small>
        </h1>
        <p className="pt-5 pb-5">
          {`Just came here for the first time and wonder how to get started?
          Head over to `}
          <a href="https://docs.pantahub.com">
            our docs
          </a>
          {` and follow one of the "Getting Started" guides to get going quickly.
          Let us know about what you have done and how things could be better.`}
        </p>
        <div className="row hts__steps pb-4">
          <div className="col-md-12 pb-4">
            <h1 className="text-center">How to start?</h1>
          </div>
          <div className="col-md-12 mt-4 mb-4">
            <div className="row">
              <div className="col-md-3">
                <div className="text-center hts__step-circle">
                  <span>1</span>
                </div>
              </div>
              <div className="col-md-9">
                <h2 className="font-weight-light mt-2">Install PVR</h2>
                <p>
                  Our main development tool is called PVR and it lets you interact with your
                  Pantavisor-enabled device remotely through Pantahub.
                </p>
                <p>
                  This CLI will be what you use to clone your device as well as post,
                  among other operations. You can download it from the following locations:
                </p>
                <pre className="bg-light pl-2 pr-2 pt-2 pb-2"><code>curl -o- -L https://git.io/fjEI1 | bash</code></pre>
              </div>
            </div>
          </div>
          <div className="col-md-12 mt-4 mb-4">
            <div className="row">
              <div className="col-md-3">
                <div className="text-center hts__step-circle">
                  <span>2</span>
                </div>
              </div>
              <div className="col-md-9">
                <h2 className="font-weight-light mt-2">Flash Pantavisor</h2>
                <p>
                  Pantavisor is the Linux device init system that
                  turns all the runtime into a set of container micro functions.
                </p>
                <p>
                  You can build you own image from scratch using this
                  <a href="https://docs.pantahub.com/environment-setup/"> guide</a>.
                  Or you can follow one of these guides depending on your hardware:
                </p>
                <ul>
                  <li><a href="https://docs.pantahub.com/image-setup-rpi3/" rel="noopener noreferrer" target="_blank">Getting started on Raspberry Pi 3</a></li>
                  <li><a href="https://docs.pantahub.com/limesdr-initial-devices/" rel="noopener noreferrer" target="_blank">Getting started with LimeSDR on Raspberry Pi 3</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div className="col-md-12 mt-4 mb-4">
            <div className="row">
              <div className="col-md-3">
                <div className="text-center hts__step-circle">
                  <span>3</span>
                </div>
              </div>
              <div className="col-md-9">
                <h2 className="font-weight-light mt-2">Claim your device</h2>
                <p>We can then use the PVR tool to claim with the user account.</p>
                <pre className="bg-light pl-2 pr-2 pt-2 pb-2"><code>pvr scan</code></pre>
                <pre className="bg-light pl-2 pr-2 pt-2 pb-2"><code>pvr claim -c CHALLENGE https://api.pantahub.com/devices/DEVICE-ID</code></pre>
                <p>
                  <img
                    src="https://docs.pantahub.com/images/unclaimed-pvr-windows.png"
                    alt="pvr scan example"
                    style={{ 'width': '100%' }}
                  />
                </p>
                <p>
                  <Link
                    className="btn btn-dark btn-xs"
                    to={`${userDashboardPath}/${username}/claim`}
                  >
                    Claim a Device
                  </Link>
                </p>
                <p>See our <a href="https://docs.pantahub.com/claim-device-auto/" rel="noopener noreferrer" target="_blank">documentation</a> for more info</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
