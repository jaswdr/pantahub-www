/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { useState, useEffect } from 'react'

import { connect } from 'react-redux'

import Recaptcha from 'react-recaptcha'

import Loading from '../Loading/Loading'

import {
  setUsername,
  setPassword,
  setEmail,
  signUp
} from '../../store/auth/actions'

import useFormValidation from '../../hooks/useFormValidation'

import useCaptcha from '../../hooks/useCaptcha'
import { parseImplicitData } from '../../lib/api.helpers'

function InnerSignUpForm (props) {
  const [encryptedAccount, setEncryptedAccount] = useState()
  const {
    onChangeUsername,
    onChangeEmail,
    onChangePassword,
    onSignUp,
    username,
    email,
    password,
    signup
  } = props
  const { loading, response, error } = signup
  const {
    formState,
    onSubmit,
    onInput,
    getError
  } = useFormValidation()
  const {
    captchaRef,
    captchaToken,
    onloadCallback,
    verifyCallback,
    expiredCallback
  } = useCaptcha()

  useEffect(() => {
    const account = parseImplicitData(window.location.hash).account
    if (account && account !== '') {
      setEncryptedAccount(account)
    }
  }, [])

  const submit = (event) => {
    const { isValid } = onSubmit(event)
    if (isValid && captchaToken) {
      onSignUp(username, email, password, captchaToken, encryptedAccount)
    }
  }

  const wasValidatedClass = formState.wasValidated ? 'was-validated' : null

  const form =
    response === null ? (
      <React.Fragment>
        {error && (
          <p key="errors" className="alert alert-danger">
            {error['Error'] || error['message'] || JSON.stringify(error)}
          </p>
        )}
        <form
          key="form"
          onSubmit={submit}
          className={wasValidatedClass}
          noValidate
        >
          {(!encryptedAccount || error) && (
            <React.Fragment>
              <div className="form-group">
                <label htmlFor="username">Username</label>
                <input
                  type="text"
                  autoCapitalize="none"
                  className="form-control"
                  id="username"
                  name="username"
                  required
                  onInput={onInput}
                  onChange={onChangeUsername}
                  placeholder="Username"
                />
                <div className="invalid-feedback"> {getError('username')} </div>
              </div>
              <div className="form-group">
                <label htmlFor="email">Email address</label>
                <input
                  type="email"
                  className="form-control"
                  name="email"
                  id="email"
                  required
                  onInput={onInput}
                  onChange={onChangeEmail}
                  placeholder="me@example.com"
                />
                <div className="invalid-feedback"> {getError('email')} </div>
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input
                  type="password"
                  id="password"
                  name="password"
                  required
                  onInput={onInput}
                  className="form-control"
                  onChange={onChangePassword}
                  placeholder="Password"
                />
                <div className="invalid-feedback"> {getError('password')} </div>
              </div>
            </React.Fragment>
          )}
          <div className="form-row d-flex justify-content-center pt-2 pb-4">
            <Recaptcha
              sitekey="6LfEzJoUAAAAADYzhzSzQIQEuBpGdp9T3Dmpkt2w"
              type="explicit"
              ref={captchaRef}
              onloadCallback={onloadCallback}
              verifyCallback={verifyCallback}
              expiredCallback={expiredCallback}
            />
          </div>
          <button
            type="submit"
            className="btn btn-dark btn-lg btn-block"
            disabled={!captchaToken}
          >
            Sign Up
          </button>
        </form>
      </React.Fragment>
    ) : (
      <p key="thanks">
        Thanks for registering to use PantaHub! Your account request will be
        reviewed and you will get an email notification with the details to
        verify your account.
      </p>
    )

  return loading ? <Loading key="loading" /> : form
}

export default connect(
  state => state.auth,
  dispatch => ({
    onChangeUsername: evt => dispatch(setUsername(evt.target.value)),
    onChangePassword: evt => dispatch(setPassword(evt.target.value)),
    onChangeEmail: evt => dispatch(setEmail(evt.target.value)),
    onSignUp: (nick, email, password, captchaToken, encrypted) => dispatch(signUp(nick, email, password, captchaToken, encrypted))
  })
)(InnerSignUpForm)
