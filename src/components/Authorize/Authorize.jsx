/* eslint-disable camelcase */
/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'

import { connect } from 'react-redux'

import { oauth2AuthorizeToken } from '../../store/oauth2/actions'

import './authorize.scss'

const availableScopes = new Map([
  ['*', 'Full Access to Pantahub Base'],
  ['devices_read', 'Access to read device info - without secrets'],
  ['prn:pantahub.com:apis:/base/all', 'Full Access to Pantahub Base'],
  ['prn:pantahub.com:apis:/fleet/all', 'Access to all your pantahub devices'],
  ['prn:pantahub.com:apis:/base/devices', 'Access to all your pantahub devices'],
  ['prn:pantahub.com:apis:/base/trails', 'Access to all your pantahub trails']
])

// const client_logos = {
//   "pantahub.com:auth:/client1": require("../../assets/images/pantacor-brand.png"),
//   default: require("../../assets/images/pantacor-brand.png")
// }

function getUrlData (url) {
  const urlParams = new URLSearchParams(url)
  return {
    client_id: urlParams.get('client_id'),
    redirect_uri: urlParams.get('redirect_uri'),
    scope: urlParams.get('scope'),
    state: urlParams.get('state'),
    type: urlParams.get('response_type'),
    response_type: urlParams.get('response_type')
  }
}

function getErrorMessage (errorObj) {
  return errorObj['Error'] ||
    (typeof errorObj['message'] === 'string' &&
    errorObj['message']) ||
    JSON.stringify(errorObj)
}

function ErrorMessage ({ error }) {
  return !error
    ? null
    : (
      <p key="errors" className="alert alert-danger">
        {getErrorMessage(error)}
      </p>
    )
}

function LoadingIcon ({ loading }) {
  return loading ? (
    <span className="mdi mdi-refresh pantahub-loading"></span>
  ) : null
}

function ScopeItems ({ scopes }) {
  return scopes.map((s, index) => {
    return <div className="item" key={index}>{availableScopes.get(s)}</div>
  })
}

function ClientLogo ({ client_id = '' }) {
  const clientId = client_id.replace('prn:', '')
  return (
    <div className="logo mt-5">
      <i className="mdi mdi-lock-open"></i>
      <span className="client-id">{clientId}</span>
    </div>
  )
}

class Oauth2Authorize extends Component {
  componentDidUpdate () {
    if (this.props.oauth2.redirect_uri) {
      window.location = this.props.oauth2.redirect_uri
    }
  }

  render () {
    const { oauth2, dispatch, auth, location } = this.props
    const { client_id, scope, redirect_uri, state, response_type = 'token' } = getUrlData(location.search)
    const submit = (evnt) => {
      evnt.preventDefault()
      dispatch(oauth2AuthorizeToken({ client_id, scope, redirect_uri, state, response_type, token: auth.token }))
    }
    const deny = (evnt) => {
      evnt.preventDefault()
      window.location = encodeURI(`${redirect_uri}#error=access_denied&state=${state}`)
    }
    return (
      <div className="container oauth2-authorize">
        <div className="row justify-content-center">
          <div className="col-md-8">
            <div className="form-registration-box">
              <ClientLogo client_id={client_id} />
              <div className="permissions-title">Would Like to:</div>
              <div className="card">
                <div className="card-body">
                  <ErrorMessage error={oauth2.oauth2Error} />
                  <form onSubmit={submit}>
                    <div className="scopes">
                      <ScopeItems scopes={scope.split(' ')} />
                    </div>
                    <div className="controls">
                      <button
                        className="btn btn-secondary"
                        disabled={oauth2.loading}
                        onClick={deny}
                      >
                          Deny
                      </button>
                      <button
                        type="submit"
                        className="btn btn-primary"
                        disabled={oauth2.loading}
                      >
                        Grant <LoadingIcon loading={oauth2.loading} />
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  state => ({
    oauth2: state.oauth2,
    auth: state.auth
  })
)(Oauth2Authorize)
