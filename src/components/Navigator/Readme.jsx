/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'

import ReactMarkdown from 'react-markdown'

import Loading from '../Loading/Loading'

import { deviceFetchFile } from '../../store/devices/actions'

class UserDeviceNavigatorReadme extends Component {
  state = {
    filename: ''
  };

  componentDidMount () {
    const { dispatch, device, token } = this.props
    const { state, rawState } = (device.history || {}).currentStep || {}
    // FIXME: improve readme filename regex
    const [readmeRawKey] =
      Object.entries(rawState || {}).find(
        ([key, value]) => key.match(/README\.md/) && value
      ) || []

    if (readmeRawKey) {
      const readmeState = state[readmeRawKey]
      if (readmeState && readmeState.url) {
        this.setState({ filename: readmeRawKey })
        dispatch(
          deviceFetchFile(token, device.id, readmeState.url, readmeRawKey)
        )
      }
    }
  }

  render () {
    const { navigator, device } = this.props
    const { filename } = this.state
    const { files } = navigator
    const { loading, content, error } = (files[device.id] || {})[filename] || {}

    return loading ? (
      <Loading />
    ) : (
      <div className="readme-wrapper">
        <div className="readme-inner-wrapper">
          {error ? (
            <strong>There was an error fetching the README file</strong>
          ) : content ? (
            <ReactMarkdown source={content} />
          ) : (
            <p>
              A <code>README.md</code> file has not yet been added to this
              device
            </p>
          )}
        </div>
      </div>
    )
  }
}

export default UserDeviceNavigatorReadme
