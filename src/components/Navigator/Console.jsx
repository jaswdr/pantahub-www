/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component, PureComponent } from 'react'
import { connect } from 'react-redux'
import matches from 'lodash.matches'
import uniq from 'lodash.uniq'
import uniqBy from 'lodash.uniqby'

import { deviceSetLogsFilter, deviceFetchNewLogs } from '../../store/devices/actions'

class LogEntry extends Component {
  state = {
    expanded: false
  };

  render () {
    const { entry } = this.props
    const { expanded } = this.state

    return (
      <div key={entry.id} className={`level-${entry.lvl.toLowerCase()}`}>
        <div className="log-row-header">
          {entry['time-created']} - {entry.lvl} - {entry.msg}
        </div>
        {expanded ? <div className="log-row-details" /> : null}
      </div>
    )
  }
}

class ConsoleFilters extends Component {
  state = {
    filters: {}
  };

  onChangeFilterField (field, value) {
    const { onFilterChange } = this.props
    const { filters } = this.state
    const newFilters = value
      ? { ...filters, [field]: value }
      : Object.entries(filters).reduce((acc, [k, v]) => {
        if (k !== field) {
          acc[k] = v
        }
        return acc
      }, {})
    this.setState({ filters: newFilters })
    onFilterChange(newFilters)
  }

  render () {
    const { sources, levels } = this.props

    return sources.length > 1 || levels.length > 1 ? (
      <div className="log-filters">
        <form className="form-inline">
          <label className="mb-2 mr-sm-2">Filter logs</label>
          {sources.length > 1 ? (
            <select
              className="custom-select form-control mb-2 mr-sm-2"
              onChange={evt => {
                this.onChangeFilterField.bind(this)('src', evt.target.value)
              }}
            >
              <option value="">All Sources</option>
              {sources.map(i => (
                <option key={i} value={i}>
                  {i}
                </option>
              ))}
            </select>
          ) : null}
          {levels.length > 1 ? (
            <select
              className="custom-select form-control mb-2 mr-sm-2"
              onChange={evt => {
                this.onChangeFilterField.bind(this)('lvl', evt.target.value)
              }}
            >
              <option value="">All Levels</option>
              {levels.map(i => (
                <option key={i} value={i}>
                  {i}
                </option>
              ))}
            </select>
          ) : null}
        </form>
      </div>
    ) : null
  }
}

class UserDeviceNavigatorConsole extends PureComponent {
  constructor (props) {
    super(props)
    this.wrapperRef = React.createRef()
    this.shouldScrollBottom = true
    this.interval = null
  }

  componentDidMount () {
    this.getData()
    this.interval = setInterval(() => {
      this.getData(true)
    }, process.env.REACT_APP_REFRESH_RATE || 1500)
  }

  componentWillUnmount () {
    if (this.interval !== null) {
      clearInterval(this.interval)
    }
    this.interval = null
  }

  getData (refresh = false) {
    const { token, dispatch, device } = this.props
    dispatch(deviceFetchNewLogs(token, device.id, refresh))
  }

  componentWillUpdate () {
    const wrapper = this.wrapperRef.current
    this.shouldScrollBottom =
      wrapper.scrollHeight - wrapper.clientHeight <= wrapper.scrollTop
  }

  componentDidUpdate () {
    if (this.shouldScrollBottom) {
      let wrapper = this.wrapperRef.current
      wrapper.scrollTop = wrapper.scrollHeight
    }
  }

  render () {
    const { logs, dispatch, navigator } = this.props
    const { logsFilter } = navigator

    const { sources, levels } = (logs || []).reduce(
      (acc, i) => ({
        sources: uniq([...acc.sources, i.src]).sort(),
        levels: uniq([...acc.levels, i.lvl]).sort()
      }),
      {
        sources: [],
        levels: []
      }
    )

    const filteredLogs = uniqBy(
      logsFilter !== null ? logs.filter(matches(logsFilter)) : (logs || []),
      (element) => element.id
    )

    return (
      <React.Fragment>
        <ConsoleFilters
          key="filters"
          sources={sources}
          levels={levels}
          onFilterChange={filter => dispatch(deviceSetLogsFilter(filter))}
        />
        <div className="log-wrapper" ref={this.wrapperRef} key="logs">
          {filteredLogs.map(e => (
            <LogEntry key={e.id} entry={e} />
          ))}
        </div>
      </React.Fragment>
    )
  }
}

export default connect(
  state => ({
    logs: state.devs.logs.entries
  }),
  null
)(UserDeviceNavigatorConsole)
