/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { flatObject } from '../../lib/object.helpers'

function UserDeviceNavigatorMetadataRow (props) {
  const { currentKey: key, value } = props
  return (
    <tr>
      <td className="meta-key">
        <pre>{key}</pre>
      </td>
      <td>
        <pre>{value}</pre>
      </td>
    </tr>
  )
}

function UserDeviceMetadata (props) {
  const {
    device,
    type = 'user-meta'
  } = props
  // const loading = navigator.setMeta.loading;
  const meta = device[type] || {}
  const title = type.replace('-', ' ').toUpperCase()
  const flattenMeta = flatObject(meta)

  const metaEntries = Object.entries(flattenMeta).sort((a, b) => {
    if (a[0] > b[0]) {
      return 1
    }
    if (a[0] < b[0]) {
      return -1
    }
    // a must be equal to b
    return 0
  })
  const metaEntriesCount = metaEntries.length

  return (
    <div className="device-meta">
      <h3 className="mt-1 mb-3">{title}</h3>
      <table className="table table-hover">
        <thead>
          <tr>
            <th className="meta-key">Key</th>
            <th>Value</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {metaEntries.map(([key, value]) => {
            return (
              <UserDeviceNavigatorMetadataRow
                key={key}
                currentKey={key}
                value={value}
              />
            )
          })}
          {metaEntriesCount === 0 && (
            <tr className="bg-light">
              <th className="text-center" colSpan={3}>
                Metadata is empty
              </th>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  )
}

export default UserDeviceMetadata
