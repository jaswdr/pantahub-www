/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'

import { deviceSetMeta } from '../../store/devices/actions'
import UserDeviceNavigatorMetadataRow from '../UserDeviceNavigatorMetadataRow/UserDeviceNavigatorMetadataRow'

class UserDeviceNavigatorMetadata extends Component {
  state = {
    editedMeta: null,
    editing: false,
    newKey: '',
    newValue: ''
  };

  title (type) {
    return type.replace('-', ' ').toUpperCase()
  }

  render () {
    const { device, dispatch, token, navigator, type = 'user-meta' } = this.props
    const { newKey, newValue, editedMeta, editing } = this.state
    const loading = navigator.setMeta.loading
    const meta = device[type] || {}
    const fullMeta = {
      ...meta,
      ...(editedMeta || {})
    }

    const metaEntries = Object.entries(fullMeta).sort((a, b) => {
      if (a[0] > b[0]) {
        return 1
      }
      if (a[0] < b[0]) {
        return -1
      }
      // a must be equal to b
      return 0
    })
    const metaEntriesCount = metaEntries.length

    return (
      <div className="device-meta">
        <h3 className="mt-5 mb-3">{this.title(type)}</h3>
        <table className="table table-hover">
          <thead>
            <tr>
              <th className="meta-key">Key</th>
              <th>Value</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {metaEntries.map(([key, value]) => {
              const inProgress =
                (editedMeta && editedMeta[key] && !meta[key]) ||
                (editedMeta && !editedMeta[key] && meta[key])

              return (
                <UserDeviceNavigatorMetadataRow
                  key={key}
                  inProgress={inProgress}
                  currentKey={key}
                  value={value}
                  loading={loading}
                  disabled={editing === true}
                  onEdit={edit => {
                    this.setState({
                      editing: edit
                    })
                  }}
                  saveHandler={(oldKey, newKey, oldValue, newValue) => {
                    delete fullMeta[oldKey]
                    fullMeta[newKey] = newValue
                    this.setState({
                      editedMeta: fullMeta,
                      editing: false
                    })
                    dispatch(deviceSetMeta(token, device.id, fullMeta, type))
                  }}
                  onDelete={key => {
                    delete fullMeta[key]
                    this.setState({
                      editedMeta: fullMeta
                    })
                    dispatch(deviceSetMeta(token, device.id, fullMeta, type))
                  }}
                />
              )
            })}
            {metaEntriesCount === 0 && (
              <tr className="bg-light">
                <th className="text-center" colSpan={3}>
                  Metadata is empty
                </th>
              </tr>
            )}
            <tr className="new-entry-header">
              <td colSpan={3}>
                <label>Add a new entry</label>
              </td>
            </tr>
            <tr className="new-entry-form">
              <td>
                <input
                  type="text"
                  className="form-control my-1 mr-sm-2"
                  disabled={loading}
                  aria-label="Metadata Field Label"
                  onChange={evt => this.setState({ newKey: evt.target.value })}
                  value={newKey}
                />
              </td>
              <td>
                <textarea
                  className="form-control my-1 mr-sm-2 textarea"
                  disabled={loading}
                  aria-label="Metadata Field Value"
                  onChange={evt => this.setState({ newValue: evt.target.value })}
                  rows={Math.max(newValue.split('\n').length, 1)}
                  value={newValue}
                />
              </td>
              <td>
                <button
                  type="button"
                  className={`btn btn-primary btn-sm my-1 mr-sm-2 ${
                    loading ? 'disabled' : ''
                  }`}
                  disabled={loading}
                  onClick={() => {
                    const editedMeta = {
                      ...fullMeta,
                      [newKey]: newValue
                    }
                    this.setState({
                      newKey: '',
                      newValue: '',
                      editedMeta
                    })
                    if (newKey && newValue) { dispatch(deviceSetMeta(token, device.id, editedMeta, type)) }
                  }}
                  title="Add"
                >
                  <span className="mdi mdi-plus" />
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}

export default UserDeviceNavigatorMetadata
