/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import sortBy from 'lodash.sortby'

import { Link } from 'react-router-dom'

import { userDashboardPath } from '../../router/routes'
import { classForDevStatus } from '../../lib/utils'

class UserDeviceNavigatorHistory extends Component {
  render () {
    const { device, username } = this.props

    return (
      <table className="table">
        <thead>
          <tr>
            <th>Revision</th>
            <th>Timestamp</th>
            <th>Status</th>
            <th>Commit Message</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {sortBy((device.history || {}).steps || [], [s => -1 * s.rev]).map(
            s => {
              const current =
                s.rev === ((device.history || {}).currentStep || {}).rev
              const status = (s['progress'] || {}).status || ''
              const timestamp = new Date(s['step-time']).toLocaleString()
              return (
                <tr
                  key={s.rev}
                  className={current ? 'bg-light font-weight-bold' : ''}
                >
                  <td className="text-center">
                    {s.rev}
                    {current ? '*' : ''}
                  </td>
                  <td>{timestamp}</td>
                  <td>
                    <span
                      className={`badge badge-${classForDevStatus(status)}`}
                    >
                      {status}
                    </span>
                  </td>
                  <td>{s['commit-msg'] || ''}</td>
                  <td>
                    <Link
                      to={`${userDashboardPath}/${username}/devices/${
                        device.id
                      }/step/${s.rev}`}
                      className="btn btn-sm btn-outline-dark"
                    >
                      <i className="mdi mdi-chevron-right" aria-hidden="true" />
                    </Link>
                  </td>
                </tr>
              )
            }
          )}
        </tbody>
      </table>
    )
  }
}

export default UserDeviceNavigatorHistory
