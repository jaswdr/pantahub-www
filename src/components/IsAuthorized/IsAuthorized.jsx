import React from 'react'
import { connect } from 'react-redux'

function IsAuthorized ({ owner, publicResource = false, auth, children }) {
  if (owner === auth.prn || publicResource) {
    return (
      <React.Fragment>
        { children }
      </React.Fragment>
    )
  } else {
    return null
  }
}

export default connect(
  state => ({ auth: state.auth }),
  null
)(IsAuthorized)
