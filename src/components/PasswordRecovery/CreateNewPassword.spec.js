import React from 'react'
import { shallow, mount } from 'enzyme'
import { CreateNewPassword, NOT_EQUAL_ERROR } from './CreateNewPassword'

const TOKEN = 'TOKEN'

describe('CreateNewPassword', () => {
  it('Loading component', () => {
    const vm = shallow(<CreateNewPassword />)
    expect(vm.exists()).toBe(true)
  })

  it('Password and confirm password are need it', () => {
    const vm = mount(<CreateNewPassword token={TOKEN} />)
    const passwordInput = vm.find('[name="password"]').first()
    const confirmPasswordInput = vm.find('[name="confirmPassword"]').first()

    expect(passwordInput.exists()).toBe(true)
    expect(confirmPasswordInput.exists()).toBe(true)
  })

  it('Password and confirm password are required', () => {
    const vm = mount(<CreateNewPassword token={TOKEN} />)
    vm.find('form').first().simulate('submit')

    const errors = vm.find('.invalid-feedback')
    expect(errors.length).toBe(2)
    expect(errors.at(0).text()).toBe(' Constraints not satisfied ')
    expect(errors.at(1).text()).toBe(' Constraints not satisfied ')
  })

  it('If password are differents show error', () => {
    const onSave = jest.fn()
    const onFailure = jest.fn()
    const vm = mount(<CreateNewPassword
      token={TOKEN}
      password="123456"
      confirmPassword="123123"
      onSave={onSave}
      onFailure={onFailure}
    />)

    vm.find('form').first().simulate('submit')

    expect(onFailure.mock.calls[0][0]).toBe(NOT_EQUAL_ERROR)
  })

  it('if password are equal call save password function', () => {
    const onSave = jest.fn()
    const onFailure = jest.fn()
    const vm = mount(<CreateNewPassword
      token={TOKEN}
      password="123456"
      confirmPassword="123456"
      onSave={onSave}
      onFailure={onFailure}
    />)

    vm.find('form').first().simulate('submit')

    expect(onFailure.mock.calls.length).toBe(0)
    expect(onSave.mock.calls[0]).toEqual(['TOKEN', '123456'])
  })
})
