import React, { useRef } from 'react'
import { connect } from 'react-redux'
import useFormValidation from '../../hooks/useFormValidation'
import TextInput from '../atoms/TextInput/TextInput'
import { setNewPassword, setNewPasswordFailure } from '../../store/auth/actions'

import './create_new_password.scss'

export const NOT_EQUAL_ERROR = 'The new password doesn\'t match with the confirmation'

export function CreateNewPassword ({
  token,
  password,
  confirmPassword,
  loading = false,
  error,
  onSave,
  onFailure
}) {
  const {
    formState,
    onSubmit,
    onInput,
    getError
  } = useFormValidation()

  const formRef = useRef()

  const submit = (event) => {
    event.preventDefault()
    const { isValid, data } = onSubmit(event)
    if (data.password !== data.confirmPassword) {
      return onFailure(NOT_EQUAL_ERROR)
    }
    if (isValid) {
      onSave(token, data.password)
    }
  }
  const baseClass = 'cnp__form'
  const wasValidatedClass = formState.wasValidated ? `${baseClass} was-validated` : baseClass

  return (
    <section className="cnp col-sm-6">
      <form
        onSubmit={submit}
        noValidate={true}
        className={wasValidatedClass}
        ref={formRef}
      >
        <div className="form-row mt-1 pt-1 justify-content-center">
          <div className="col-12">
            <TextInput
              className="form-control"
              name="password"
              type="password"
              placeholder="new password"
              minLength="6"
              required={true}
              error={getError('password')}
              onInput={onInput}
              defaultValue={password}
            />
          </div>
        </div>
        <div className="form-row mt-1 mt-1 justify-content-center">
          <div className="col-12">
            <TextInput
              className="form-control"
              name="confirmPassword"
              type="password"
              placeholder="confirm password"
              minLength="6"
              required={true}
              error={getError('confirmPassword')}
              onInput={onInput}
              defaultValue={confirmPassword}
            />
          </div>
        </div>
        <div className="form-row pt-2 justify-content-center">
          <div className="col-12 text-center">
            <div className="text-danger"> {error} </div>
          </div>
          <div className="col-auto">
            <button
              type="submit"
              className="btn btn-dark rnp__submit"
              disabled={loading || !formState.isValid}
            >
              {loading && (<span className="mdi mdi-refresh pantahub-loading" />)}
              Save new password
            </button>
          </div>
        </div>
      </form>
    </section>
  )
}

export default connect(
  (state, ownProps) => ({
    ...state.auth.setPassword,
    ...ownProps
  }),
  {
    onSave: setNewPassword,
    onFailure: setNewPasswordFailure
  }
)(CreateNewPassword)
