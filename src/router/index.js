/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React from 'react'
import { Route, Switch } from 'react-router'

// Application components
import { userIsAuthenticated, userIsNotAuthenticated } from '../lib/auth'
import Nav from '../components/Nav/Nav'
import Footer from '../components/Footer/Footer'
import Loading from '../components/Loading/Loading'
import * as routes from './routes'
import { withOwnerTracker } from '../store/ga-middleware'

// Pages components
const ClaimDevice = React.lazy(() => import('../components/ClaimDevice/ClaimDevice'))
const Login = React.lazy(() => import('../components/Login/Login'))
const TermsOfService = React.lazy(() => import('../components/TermsOfService/TermsOfService'))
const UserDashboard = React.lazy(() => import('../components/UserDashboard/UserDashboard'))
const UserDevices = React.lazy(() => import('../components/UserDevices/UserDevices'))
const UserAccount = React.lazy(() => import('../components/UserAccount/UserAccount'))
const VerifyAccount = React.lazy(() => import('../components/VerifyAccount/VerifyAccount'))
const GeneralError = React.lazy(() => import('../components/GeneralError/GeneralError'))
const Oauth2Authorize = React.lazy(() => import('../components/Authorize/Authorize'))
const UserDevice = React.lazy(() => import('../components/UserDevice'))
const LandingPage = React.lazy(() => import('../pages/LandingPage/LandingPage'))
const AboutUsPage = React.lazy(() => import('../pages/AboutUsPage/AboutUsPage'))
const PrivacyPage = React.lazy(() => import('../pages/PrivacyPage/PrivacyPage'))
const RequestNewPasswordPage = React.lazy(() => import('../pages/RequestNewPasswordPage/RequestNewPasswordPage'))
const ResetPasswordPage = React.lazy(() => import('../pages/ResetPasswordPage/ResetPasswordPage'))
const SignUp = React.lazy(() => import('../pages/SignUpPage/SignUpPage'))

// Routing layouts
export const Oauth2Layout = props => (
  <main className="container">
    <Route
      exact
      path={`${props.match.path}/authorize`}
      component={Oauth2Authorize}
    />
    <br />
    <br />
  </main>
)

export const FullHeightLoading = () => (
  <div style={{ height: '90vh', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
    <Loading />
  </div>
)

export const UserLayout = props => (
  <main className="container">
    <Route
      exact
      path={`${props.match.path}/:username`}
      component={withOwnerTracker(UserDashboard, 'user/dashboard')}
    />
    <Route
      exact
      path={`${props.match.path}/:username/devices`}
      component={withOwnerTracker(UserDevices, 'user/devices')}
    />
    <Route
      exact
      path={`${props.match.path}/:username/devices/:deviceId`}
      component={withOwnerTracker(UserDevice, 'user/device')}
    />
    <Route
      exact
      path={`${props.match.path}/:username/devices/:deviceId/step/:revision`}
      component={withOwnerTracker(UserDevice, 'user/device/revision') }
    />
    <Route
      exact
      path={`${props.match.path}/:username/claim`}
      component={withOwnerTracker(ClaimDevice, 'user/claim')}
    />
    <Route
      exact
      path={`${props.match.path}/:username/account`}
      component={withOwnerTracker(UserAccount, 'user/account')}
    />
    <br />
    <br />
  </main>
)

export const RouterLayout = () => (
  <div>
    <header>
      <Nav />
    </header>
    <React.Suspense fallback={<FullHeightLoading />}>
      <GeneralError />
      <Switch>
        <Route
          exact
          path={routes.landingPath}
          component={LandingPage}
        />
        <Route
          exact
          path={routes.tosPath}
          component={TermsOfService}
        />
        <Route
          exact
          path={routes.aboutUs}
          component={AboutUsPage}
        />
        <Route
          path={routes.userDashboardPath}
          component={userIsAuthenticated(UserLayout)}
        />
        <Route
          path={routes.oauth2Path}
          component={userIsAuthenticated(Oauth2Layout)}
        />
        <Route
          exact
          path={routes.loginPath}
          component={userIsNotAuthenticated(Login)}
        />
        <Route
          exact
          path={routes.signUpPath}
          component={userIsNotAuthenticated(SignUp)}
        />
        <Route
          exact
          path={routes.verifyPath}
          component={userIsNotAuthenticated(VerifyAccount)}
        />
        <Route
          exact
          path={routes.privacyPath}
          component={PrivacyPage}
        />
        <Route
          exact
          path={routes.requestNewPassword}
          component={RequestNewPasswordPage}
        />
        <Route
          exact
          path={routes.resetPassword}
          component={ResetPasswordPage}
        />
      </Switch>
    </React.Suspense>
    <footer>
      <Footer />
    </footer>
  </div>
)
